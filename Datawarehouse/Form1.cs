﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DataContext;
using Datawarehouse;

namespace Database
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void DisableButtons()
        {
            btnDelete.Enabled = btnCreate.Enabled = btnInsert.Enabled = false;
        }

        public static void InvokeDgv(int row, int num)
        {
            if (View.InvokeRequired)
            {
                View.Invoke(new Action(() =>
                {
                    View.Rows[row].Cells[1].Value = num;
                }));
            }
            else
            {
                View.Rows[row].Cells[1].Value = num;
            }
        }

        private void EnableButtons()
        {
            btnDelete.Enabled = btnCreate.Enabled = btnInsert.Enabled = true;
        }

        private void WriteStatus(string status)
        {
            if (txtStatus.InvokeRequired)
            {
                txtStatus.Invoke(new Action(() =>
                    {
                        txtStatus.AppendText(status + Environment.NewLine);
                    }
                
                ));

            }
            else
            {
                txtStatus.AppendText(status + Environment.NewLine);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                using (var context = new DataContext.Database())
                {
                    var list = context.Database.Delete();
                    WriteStatus(list ? "Database Status : Deleted" : "Database Status : Unable to Deleted");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                WriteStatus(@"Error : " + exception.Message);
            }
            finally
            {
                EnableButtons();
                WriteStatus("Processing Complete");
            }
            
        }

        public static DataGridView View;
        private void btnInsert_Click(object sender, EventArgs e)
        {
            var context = new DataContext.Database();
            View = dgvTable;
            dgvTable.Rows.Clear();
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                {

                    dgvTable.Rows.Add("Brands", "0");
                    dgvTable.Rows.Add("Categories", "0");
                    dgvTable.Rows.Add("SubCategories", "0");
                    dgvTable.Rows.Add("UserAccount", "0");
                    dgvTable.Rows.Add("CreditCard", "0");
                    dgvTable.Rows.Add("Admin", "0");
                    dgvTable.Rows.Add("Shipper", "0");
                    dgvTable.Rows.Add("Supplier", "0");
                    dgvTable.Rows.Add("Color", "0");
                    dgvTable.Rows.Add("Product", "0");
                    dgvTable.Rows.Add("PurchaseOrder", "0");
                    dgvTable.Rows.Add("SaleOrder", "0");
                    dgvTable.Rows.Add("Invoices", "0");
                    var allThread = new Thread(() =>
                        {
                            try
                            {
                                Seeder.Brands(0);
                                WriteStatus("Database Status : Rows Added To Brands [" +
                                            context.Brands.LongCount() + "]");
                                Seeder.Categories(1);
                                WriteStatus("Database Status : Rows Added To Categories [" +
                                            context.Categories.LongCount() + "]");
                                Seeder.SubCategories(2);
                                WriteStatus("Database Status : Rows Added To SubCategories [" +
                                            context.SubCategories.LongCount() + "]");
                                Seeder.UserAccounts(3);
                                WriteStatus("Database Status : Rows Added To UserAccounts [" +
                                            context.UserAccounts.LongCount() + "]");
                                Seeder.CreditCardNumbers(4);
                                WriteStatus("Database Status : Rows Added To CreditCards [" +
                                            context.CreditCards.LongCount() + "]");
                                Seeder.Admin(5);
                                WriteStatus("Database Status : Rows Added To Admin [" +
                                            context.Administrators.LongCount() + "]");
                                Seeder.Shipper(6);
                                WriteStatus("Database Status : Rows Added To Shipper [" +
                                            context.Shippings.LongCount() + "]");

                                Seeder.Supplier(7);
                                WriteStatus("Database Status : Rows Added To Supplier [" +
                                            context.Suppliers.LongCount() + "]");
                                Seeder.ProductOptions();
                                Seeder.Colors(8);
                                WriteStatus("Database Status : Rows Added To Colors [" +
                                            context.Colors.LongCount() + "]");
                                Seeder.Product(9, context.Administrators.Count(), context.Classes.Count(),
                                    context.Colors.Count(),
                                    context.Sizes.Count(), context.Styles.Count(), context.SubCategories.Count(),context.Brands.Count());
                                WriteStatus("Database Status : Rows Added To Product [" +
                                            context.Products.LongCount() + "]");
                                Seeder.PurchaseOrder(10, context.Administrators.Count(), context.Suppliers.Count(),
                                    context.Products.Count());
                                WriteStatus("Database Status : Rows Added To PurchaseOrder [" +
                                            context.PurchaseOrders.LongCount() + "]");
                                Seeder.SaleOrder(11, context.CreditCards.Count(), context.Shippings.Count(),
                                    context.Products.Count());
                                WriteStatus("Database Status : Rows Added To SaleOrder [" +
                                            context.SaleOrders.LongCount() + "]");
                                Seeder.Invoice(12, context.SaleOrders.Count(), context.Shippings.Count());
                                WriteStatus("Database Status : Rows Added To Invoice [" +
                                            context.Invoices.LongCount() + "]");
                                context.Dispose();
                                EnableButtons();
                                WriteStatus("Processing Completed");

                            }
                            catch (Exception exception)
                            {
                                MessageBox.Show(exception.Message);
                            }
                        }
                    );

                    allThread.Start();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                WriteStatus(@"Error : " + exception.Message);
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                using (var context = new DataContext.Database())
                {

                    context.Database.Create();
                    WriteStatus("Database Status : Created");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                WriteStatus("Database Status : Database not Created");
            }
            finally
            {
                EnableButtons();
                WriteStatus("Processing Complete");
            }
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            try
            {
                using (var context = new DataContext.Database())
                {
                    WriteStatus("Database Status : Rows Added To Brands [" +
                                context.Brands.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Categories [" +
                                context.Categories.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To SubCategories [" +
                                context.Categories.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To UserAccounts [" +
                                context.UserAccounts.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To CreditCards [" +
                                context.CreditCards.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Admin [" +
                                context.Administrators.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Shipper [" +
                                context.Shippings.LongCount() + "]");

                    WriteStatus("Database Status : Rows Added To Supplier [" +
                                context.Suppliers.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Colors [" +
                                context.Colors.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Product [" +
                                context.Products.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To PurchaseOrder [" +
                                context.PurchaseOrders.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To SaleOrder [" +
                                context.SaleOrders.LongCount() + "]");
                    WriteStatus("Database Status : Rows Added To Invoice [" +
                                context.Invoices.LongCount() + "]");


                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            finally
            {
                WriteStatus("Processing Completed");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var context = new DataContext.Database();
            View = dgvTable;
            dgvTable.Rows.Clear();
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                {
                    dgvTable.Rows.Clear();

                    dgvTable.Rows.Add("Brands", "0");
                    dgvTable.Rows.Add("Categories", "0");
                    dgvTable.Rows.Add("SubCategories", "0");
                    dgvTable.Rows.Add("UserAccount", "0");
                    dgvTable.Rows.Add("CreditCard", "0");
                    dgvTable.Rows.Add("Admin", "0");
                    dgvTable.Rows.Add("Shipper", "0");
                    dgvTable.Rows.Add("Supplier", "0");
                    dgvTable.Rows.Add("Color", "0");
                    dgvTable.Rows.Add("Product", "0");
                    dgvTable.Rows.Add("PurchaseOrder", "0");
                    dgvTable.Rows.Add("SaleOrder", "0");
                    dgvTable.Rows.Add("Invoices", "0");
                    var allThread = new Thread(() =>
                    {
                        try
                        {
                            Seeder.SaleOrder(11, context.CreditCards.Count(), context.Shippings.Count(),
                                context.Products.Count());
                            WriteStatus("Database Status : Rows Added To SaleOrder [" +
                                        context.SaleOrders.LongCount() + "]");
                            Seeder.Invoice(12, context.SaleOrders.Count(), context.Shippings.Count());
                            WriteStatus("Database Status : Rows Added To Invoice [" +
                                        context.Invoices.LongCount() + "]");
                            context.Dispose();
                            EnableButtons();
                            WriteStatus("Processing Completed");

                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message);
                        }
                    }
                    );

                    allThread.Start();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                WriteStatus(@"Error : " + exception.Message);
            }
        }

        private void btn_DWDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                using (var context = new DataContext.Datawarehouse())
                {

                    context.Database.Delete();
                    WriteStatus("Datawarehouse Status : Delete");
                }
            }
            catch (Exception exception)
            {
               Console.WriteLine(exception);
               WriteStatus("Datawarehouse Status : Database not Deleted");
            }
            finally
            {
                EnableButtons();
                WriteStatus("Processing Complete");
            }
        }

        private void btn_DWCreate_Click(object sender, EventArgs e)
        {
            try
            {
                DisableButtons();
                WriteStatus("Processing");
                using (var context = new DataContext.Datawarehouse())
                {

                    context.Database.Create();
                    WriteStatus("Datawarehouse Status : Created");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                WriteStatus("Datawarehouse Status : Database not Created");
            }
            finally
            {
                EnableButtons();
                WriteStatus("Processing Complete");
            }
        }
    }
}
