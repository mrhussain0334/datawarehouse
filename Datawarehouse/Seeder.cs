﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Database;
using DataContext.Model;

namespace Datawarehouse
{
    public class Seeder
    {

        private const string BrandsFile = @"Data/Brands.csv";
        private const string CategoriesFile = @"Data/Categories.csv";
        private const string SubCategoriesFile = @"Data/SubCategories.csv";
        private const string UserFile = @"Data/User.csv";
        private const string CreditCardNumberFile = @"Data/CreditCardNumbers.csv";
        private const string AdminFile = @"Data/Admin.csv";
        private const string ShipperFile = @"Data/Shipping.csv";
        private const string SupplierFile = @"Data/Supplier.csv";
        private const string ColorFile = @"Data/Color.csv";
        private const string ProductFile = @"Data/Products.csv";
        private const string InvoiceFIle = @"Data/Invoices.csv";
        private const int Count = 2000;
        private static readonly Random Rand = new Random((int)DateTime.Now.Ticks);
        private static readonly Dictionary<long, int> Dictionary = new Dictionary<long, int>();
        private static readonly DateTime From = new DateTime(2000, 1, 1);
        private static readonly DateTime To = new DateTime(2017, 1, 1);
        private static TimeSpan _range;
        private static DateTime RandomDate
        {
            get
            {
                _range = From - To;
                var randTimeSpan = new TimeSpan((long)(Rand.NextDouble() * _range.Ticks));
                var x = From + randTimeSpan.Duration();
                return (x);
            }
        }

        private static DataContext.Database CheckContext(DataContext.Database context, int i)
        {

            var db = new DataContext.Database();
            try
            {
                if (i % Count != 0) return context;
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = false;
            }
            return db;
        }

        public static void Brands(int row)
        {
            var i = 1;
            try
            {
                var context = new DataContext.Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                using (var reader = new CsvReader(new CsvParser(new StreamReader(BrandsFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var brand = new Brand()
                        {
                            Id = i,
                            Name = reader.GetField<string>(0),
                            City = reader.GetField<string>(1),
                            Country = reader.GetField<string>(2),
                            State = reader.GetField<string>(3),
                            PostCode = reader.GetField<string>(4),
                            Address = reader.GetField<string>(5)
                        };
                        brand.Email = brand.Name.Replace(" ","").Trim() + "@gmail.com";
                        context.Brands.Add(brand);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SubCategories(int row)
        {
            var i = 1;
            try
            {

                var context = new DataContext.Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                using (var reader = new CsvReader(new CsvParser(new StreamReader(SubCategoriesFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var cat = new SubCategory()
                        {
                            DateCreated = DateTime.Now,
                            Id = i,
                            Name = reader.GetField<string>(1).Replace("\"","").Trim(),
                            CategoryId = reader.GetField<int>(0)
                        };
                        context.SubCategories.Add(cat);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Categories(int row)
        {
            var i = 1;
            try
            {
                var context = new DataContext.Database();
                
                using (var reader = new CsvReader(new CsvParser(new StreamReader(CategoriesFile))))
                {
                    reader.Configuration.HasHeaderRecord = false;
                    //reader.ReadHeader();
                    while (reader.Read())
                    {
                        var cat = new Category()
                        {
                            DateCreated = DateTime.Now,
                            Id = i,
                            Name = reader.GetField<string>(0).Replace("\"","").Trim()
                        };
                        context.Categories.Add(cat);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void UserAccounts(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(UserFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var user = new UserAccount()
                        {
                            Id = i,
                            UserName = reader.GetField<string>(0),
                            FirstName = reader.GetField<string>(1),
                            LastName = reader.GetField<string>(2),
                            Password = reader.GetField<string>(3),
                            Address = reader.GetField<string>(4),
                            PhoneNumber = reader.GetField<string>(5),
                            Country = reader.GetField<string>(6),
                            City = reader.GetField<string>(7),
                            State = reader.GetField<string>(8),
                            PostCode = reader.GetField<string>(9),
                            Email = reader.GetField<string>(10),
                            DateRegistered = RandomDate
                        };
                        context.UserAccounts.Add(user);
                        Form1.InvokeDgv(row,i);
                        context = CheckContext(context, i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void CreditCardNumbers(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(CreditCardNumberFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var credit = new CreditCard
                        {
                            Id = i,
                            CreditCardNumber = reader.GetField<string>(0),
                            CreditCardType = reader.GetField<string>(1),
                            Address = reader.GetField<string>(2),
                            Country = reader.GetField<string>(4),
                            City = reader.GetField<string>(5),
                            State = reader.GetField<string>(6),
                            PostCode = reader.GetField<string>(7),
                            Email = reader.GetField<string>(8),
                            UserId = (i % 5000) == 0 ? 1 : i % 5000
                        };
                        context.CreditCards.Add(credit);
                        Form1.InvokeDgv(row, i);
                        context = CheckContext(context, i);
                        i++;

                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch
                (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Admin(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(AdminFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {

                        var admin = new Administrator()
                        {
                            Id = i,
                            UserName = reader.GetField<string>(0),
                            FirstName = reader.GetField<string>(1),
                            LastName = reader.GetField<string>(2),
                            Password = reader.GetField<string>(3),
                            Address = reader.GetField<string>(4),
                            PhoneNumber = reader.GetField<string>(5),
                            Country = reader.GetField<string>(6),
                            City = reader.GetField<string>(7),
                            State = reader.GetField<string>(8),
                            PostCode = reader.GetField<string>(9),
                            Email = reader.GetField<string>(10),
                            StartDate = RandomDate,
                            Salary = Rand.Next(500,1000),
                            
                        };
                        admin.Account = new Account()
                        {
                            Id = i,
                            AdminId = i,
                            AccountHolderName = admin.FirstName + "  " + admin.LastName,
                            AccountNumber = (101010101010 + i) + "",
                            BankAddress = admin.Address,
                            BankCity = admin.City,
                            BankCountry = admin.Country,
                            BankName = "HBL",
                            BankPostCode = admin.PostCode,
                            DateofCreation = RandomDate,
                        };
                        context.Administrators.Add(admin);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Shipper(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(ShipperFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var ship = new Shipping()
                        {
                            Id = i,
                            Name = reader.GetField<string>(0),
                            Address = reader.GetField<string>(1),
                            City = reader.GetField<string>(2),
                            Country = reader.GetField<string>(3),
                            State = reader.GetField<string>(4),
                            PostCode = reader.GetField<string>(5)
                        };
                        context.Shippings.Add(ship);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Supplier(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(SupplierFile))))
                {
                    reader.ReadHeader();
                    while (reader.Read())
                    {
                        var supplier = new Supplier()
                        {
                            Id = i,
                            UserName = reader.GetField<string>(0),
                            FirstName = reader.GetField<string>(1),
                            LastName = reader.GetField<string>(2),
                            Password = reader.GetField<string>(3),
                            CompanyName = reader.GetField<string>(4),
                            Address = reader.GetField<string>(5),
                            City = reader.GetField<string>(6),
                            Country = reader.GetField<string>(7),
                            State = reader.GetField<string>(8),
                            PostCode = reader.GetField<string>(9),
                            Email = reader.GetField<string>(10),
                            PhoneNumber = "" + (101 + i) + "-" + "254" + (100 + i),
                            AccountNumber = reader.GetField<string>(11),
                        };
                        context.Suppliers.Add(supplier);
                        Form1.InvokeDgv(row,i);
                        context = CheckContext(context, i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void ProductOptions()
        {
            try
            {
                var context = new DataContext.Database();
                var i = 1;
                context.Sizes.Add(new Size()
                {
                    Id = i++,
                    Name = "Small"
                });
                context.Sizes.Add(new Size()
                {
                    Id = i++,
                    Name = "Medium"
                });
                context.Sizes.Add(new Size()
                {
                    Id = i++,
                    Name = "Large"
                });
                context.Sizes.Add(new Size()
                {
                    Id = i++,
                    Name = "Xtra-Large"
                });
                i = 1;
                context.Classes.Add(new Class()
                {
                    Id = i++,
                    Name = "Low"
                });
                context.Classes.Add(new Class()
                {
                    Id = i++,
                    Name = "High"
                });
                context.Classes.Add(new Class()
                {
                    Id = i++,
                    Name = "Medium"
                });
                i = 1;
                context.Styles.Add(new Style()
                {
                    Id = i++,
                    Name = "Male"
                });
                context.Styles.Add(new Style()
                {
                    Id = i++,
                    Name = "Female"
                });
                context.Styles.Add(new Style()
                {
                    Id = i,
                    Name = "Universal"
                });
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Colors(int row)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(ColorFile))))
                {
                    //reader.ReadHeader();
                    while (reader.Read())
                    {
                        var color = new Color()
                        {
                            Id = i,
                            Name = reader.GetField<string>(0),
                            Argb = reader.GetField<string>(1),
                        };
                        context.Colors.Add(color);
                        Form1.InvokeDgv(row,i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void Product(int row,int adminCount,int classCount,int colorCount,int sizeCount,int styleCount,int subCategory,int brandCount)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                using (var reader = new CsvReader(new CsvParser(new StreamReader(ProductFile))))
                {
                    //reader.ReadHeader();
                    while (reader.Read())
                    {
                        var product = new Product()
                        {
                            Id = i,
                            Name = reader.GetField<string>(0),
                            SKU = "" + (10000000 + i),
                            BrandId = Rand.Next(1,brandCount),
                            AdminId = Rand.Next(1,adminCount),
                            ClassId = Rand.Next(1,classCount),
                            ColorId = Rand.Next(1,colorCount),
                            CostPrice = Rand.Next(1,20),
                            SizeId = Rand.Next(1,sizeCount),
                            StyleId = Rand.Next(1,styleCount),
                            Weight = Rand.Next(4,10),
                            
                            SubCategoryId = Rand.Next(1,subCategory),
                        };
                        const double cost = 0.25;
                        product.ListPrice = (float) (product.CostPrice * (1 + cost));
                        context.Products.Add(product);
                        Form1.InvokeDgv(row, i);
                        context = CheckContext(context, i);
                        i++;
                    }

                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void PurchaseOrder(int row,int adminCount,int supplierCount,int productCount)
        {
            try
            {
                var context = new DataContext.Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var id = 1;
                var purchaseId = 1;
                for (var i = 0; i < 100000; i++)
                {
                    var order = new PurchaseOrder
                    {
                        Id = id,
                        AdminId = Rand.Next(1, adminCount),
                        DateOfPurchase = RandomDate,
                        SupplierId = Rand.Next(1, supplierCount),
                        Inventories = new List<Inventory>(),
                        PurchaseOrderDetails = new List<PurchaseOrderDetail>()
                        
                    };
                    var orderDetailCount = Rand.Next(1, 8);
                    for (var j = 0; j < orderDetailCount; j++)
                    {
                        var detail = new PurchaseOrderDetail()
                        {
                            Id = purchaseId,
                            Discount = (Rand.Next(1, 60) % 3 == 0) ? 0.25f : 0,
                            ProductId = Rand.Next(1, productCount),
                            PurchaseOrderId = id,
                            Quanity = Rand.Next(1, 20),
                        };
                        order.PurchaseOrderDetails.Add(detail);
                        var inventory = new Inventory()
                        {
                            PurchaseOrderId = purchaseId,
                            DateOfTransaction = order.DateOfPurchase,
                            ProductId = detail.ProductId,
                            QuantityAdded = detail.Quanity,
                        };
                        order.Inventories.Add(inventory);
                        purchaseId++;
                    }
                    context.PurchaseOrders.Add(order);
                    Form1.InvokeDgv(row,id);
                    context = CheckContext(context, id);
                    id++;
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void SaleOrder(int row, int creditCount, int shipperCount, int productCount)
       {
            try
            {
                var context = new DataContext.Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var saleId = 1;
                var saleDetailId = 1;
                for (var i = 0; i < 50000; i++)
                {
                    var order = new SaleOrder()
                    {
                        Id = saleId,
                        CreditCardId = Rand.Next(1, creditCount),
                        DateOfSale = RandomDate,
                        Inventories = new List<Inventory>(),
                        SaleOrderDetails = new List<SaleOrderDetail>()
                       
                    };
                    var orderDetailCount = Rand.Next(1, 8);
                    for (var j = 0; j < orderDetailCount; j++)
                    {
                        var detail = new SaleOrderDetail()
                        {

                            Id = saleDetailId,
                            Quantity = Rand.Next(2,10),
                            Discount = (Rand.Next(1, 60) % 5 == 0) ? 0.25f : 0,
                            ProductId = Rand.Next(1, productCount),
                            SaleOrderId = saleId,
                        };
                        var remaining = context.Inventories.Where(p => p.ProductId == detail.ProductId).Sum(p => p.QuantiyRemoved + p.QuantityAdded);
                        if (remaining <= 0) continue;
                        if (detail.Quantity > remaining)
                        {
                            j--;
                            continue;
                        }
                        order.SaleOrderDetails.Add(detail);
                        var inventory = new Inventory()
                        {
                            SaleOrderId = saleId,
                            DateOfTransaction = order.DateOfSale,
                            ProductId = detail.ProductId,
                            QuantiyRemoved = (-detail.Quantity),
                        };
                        order.Inventories.Add(inventory);
                        saleDetailId++;
                    }
                    if (order.SaleOrderDetails.Count > 0)
                    {
                        context.SaleOrders.Add(order);
                        context = CheckContext(context, saleId);
                        Form1.InvokeDgv(row, saleId);
                        saleId++;
                    }
                    else
                    {
                        i--;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Invoice(int row, int saleCount,int shippingCount)
        {
            try
            {
                var i = 1;
                var context = new DataContext.Database();
                using (var reader = new CsvReader(new CsvParser(new StreamReader(InvoiceFIle))))
                {
                    while (reader.Read())
                    {
                        var invoice = new Invoice()
                        {
                            Id = i,
                            SaleOrderId = i,
                            Discount = (Rand.Next(1, 60) % 5 == 0) ? 0.25f : 0,
                            ShippingId = Rand.Next(1,shippingCount),
                            FreightCost = Rand.Next(5,8),
                            ShippingAddress = reader.GetField<string>(0),
                            ShippingCountry = reader.GetField<string>(1),
                            ShippingCity = reader.GetField<string>(2),
                            ShippingState = reader.GetField<string>(3),
                            ShippingPostCode = reader.GetField<string>(4),
                            ShippingCost = Rand.Next(5,10)
                        };
                        context.Invoices.Add(invoice);
                        Form1.InvokeDgv(row,i);
                        context = CheckContext(context, i);
                        i++;
                    }
                }
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
