﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DataContext;
using DataContext.DWModel.InventoryFactCube;
using DataContext.DWModel.PurchaseFactCube;
using DataContext.DWModel.SaleFactCube;

namespace DataExtracter
{
    public partial class Form1 : Form
    {
        public Datawarehouse Dw { get; set; }
        private static DataGridView _view;
        public static void InvokeDgv(int row, int num)
        {
            if (_view.InvokeRequired)
            {
                _view.Invoke(new Action(() =>
                {
                    _view.Rows[row].Cells[1].Value = num;
                }));
            }
            else
            {
                _view.Rows[row].Cells[1].Value = num;
            }
        }
        private void WriteStatus(string status)
        {
            if (txtStatus.InvokeRequired)
            {
                txtStatus.Invoke(new Action(() =>
                {
                    txtStatus.Text = (status + Environment.NewLine);
                }

                ));

            }
            else
            {
                txtStatus.Text = (status + Environment.NewLine);
            }
        }
        public Form1()
        {
            InitializeComponent();
            Dw = new Datawarehouse();
            _view = dgvTable;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                WriteStatus("Processing");
                SetButtons(false);
                using (var context = new Datawarehouse())
                {
                    WriteStatus(context.Database.Delete()
                        ? "Datawarehouse Deleted"
                        : "Datawarehouse Could not be Deleted");
                }
            }
            catch (Exception exception)
            {
                WriteStatus("Error Could Not Delete");
                Console.WriteLine(exception);
            }
            finally
            {
                SetButtons(true);
                WriteStatus("Processing Completed");
            }
        }

        private void SetButtons(bool status)
        {
            if (btnCreate.InvokeRequired)
            {
                btnCreate.Invoke(new Action(() =>
                {
                    btnCreate.Enabled = status;
                }));
                btnDelete.Invoke(new Action(() =>
                {
                    btnDelete.Enabled = status;
                }));
                btnSeedOther.Invoke(new Action(() =>
                {
                    btnSeedOther.Enabled = status;
                }));
            }
            else
            {
                btnSeedOther.Enabled = btnCreate.Enabled = btnDelete.Enabled = status;
            }
        }
        


        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                dgvTable.Columns.Clear();
                dgvTable.Rows.Clear();
                dgvTable.Columns.Add("Table","Table");
                dgvTable.Columns.Add("Count", "Count");
                dgvTable.Rows.Add("Products","0");
                dgvTable.Rows.Add("Supplier","0");
                dgvTable.Rows.Add("Adminstrator", "0");
                dgvTable.Rows.Add("CreditCards", "0");
                dgvTable.Rows.Add("Invoices", "0");
                dgvTable.Rows.Add("Shippings", "0");
                dgvTable.Rows.Add("UserAccounts", "0");
                dgvTable.Rows.Add("SaleFact", "0");
                dgvTable.Rows.Add("PurchaseFact", "0");
                dgvTable.Rows.Add("InventoryFact", "0");
                var allThread = new Thread(Seed);
                allThread.Start();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void Status()
        {
            var dw = new Datawarehouse();
            WriteStatus("Products : " + dw.Products.Count() + " rows");
            WriteStatus("InventoryFacts : " + dw.InventoryFacts.Count() + " rows");
            WriteStatus("Supplier : " + dw.Suppliers.Count() + " rows");
            WriteStatus("Admins : " + dw.Adminstrator.Count() + " rows");
            WriteStatus("CreditCards : " + dw.CreditCards.Count() + " rows");
            WriteStatus("Invoices : " + dw.Invoices.Count() + " rows");
            WriteStatus("Shippings : " + dw.Shippings.Count() + " rows");
            WriteStatus("UserAccounts : " + dw.UserAccounts.Count() + " rows");
            WriteStatus("SaleFact : " + dw.SaleFacts.Count() + " rows");
            WriteStatus("PurchaseFact : " + dw.PurchaseFacts.Count() + " rows");
            WriteStatus("InventoryFact : " + dw.InventoryFacts.Count() + " rows");
        }

        private void Seed()
        {
            var dw = new Datawarehouse();
            try
            {
                SetButtons(false);
                WriteStatus("Transferring Products....");
                Seeder.Products(0);
                WriteStatus("Transferring Complete");
                WriteStatus("Products : " + dw.Products.Count() + " rows");
                WriteStatus("Transferring Supplier....");
                Seeder.Supplier(1);
                WriteStatus("Transferring Complete");
                WriteStatus("Supplier : " + dw.Suppliers.Count() + " rows");
                WriteStatus("Transferring Admins....");
                Seeder.Admins(2);
                WriteStatus("Transferring Complete");
                WriteStatus("Admins : " + dw.Adminstrator.Count() + " rows");
                WriteStatus("Transferring CreditCards....");
                Seeder.CreditCards(3);
                WriteStatus("Transferring Complete");
                WriteStatus("CreditCards : " + dw.CreditCards.Count() + " rows");
                WriteStatus("Transferring Invoices....");
                Seeder.Invoices(4);
                WriteStatus("Transferring Complete");
                WriteStatus("Invoices : " + dw.Invoices.Count() + " rows");
                WriteStatus("Transferring Shippings....");
                Seeder.Shippings(5);
                WriteStatus("Transferring Complete");
                WriteStatus("Shippings : " + dw.Shippings.Count() + " rows");
                WriteStatus("Transferring UserAccounts....");
                Seeder.UserAccounts(6);
                WriteStatus("Transferring Complete");
                WriteStatus("UserAccounts : " + dw.UserAccounts.Count() + " rows");
                WriteStatus("Transferring SaleFacts....");
                Seeder.SaleFact(7);
                WriteStatus("Transferring Complete");
                WriteStatus("SaleFacts : " + dw.SaleFacts.Count() + " rows");
                WriteStatus("Transferring PurchaseFacts....");
                Seeder.PurchaseFact(8);
                WriteStatus("Transferring Complete");
                WriteStatus("PurchaseFacts : " + dw.PurchaseFacts.Count() + " rows");
                WriteStatus("Transferring InventoryFacts....");
                Seeder.InventoryFact(9);
                WriteStatus("Transferring Complete");
                WriteStatus("InventoryFacts : " + dw.InventoryFacts.Count() + " rows");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
            finally
            {
                dw.Dispose();
                SetButtons(true);
            }
        }
        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                WriteStatus("Processing");
                SetButtons(false);
                using (var context = new Datawarehouse())
                {
                    context.Database.Create();
                    WriteStatus("Datawarehouse Created");
                }
            }
            catch (Exception exception)
            {
                WriteStatus("Error Could Not Created");
                Console.WriteLine(exception);
            }
            finally
            {
                SetButtons(true);
                WriteStatus("Processing Completed");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (txtStatus.InvokeRequired)
            {
                txtStatus.Invoke(new Action(() =>
                {
                    txtStatus.Text = "";
                }

                ));

            }
            else
            {
                txtStatus.Text = ("");
            }
        }
        private void btnCheck_Click(object sender, EventArgs e)
        {
            Status();
        }

        private void btnSeedOther_Click(object sender, EventArgs e)
        {
            SetButtons(false);
            dgvTable.Rows.Clear();
            dgvTable.Rows.Add("Rows", "0");
            var thread = new Thread(Conversion);
            thread.Start();
        }

        private void Conversion()
        {
            try
            {
                var context = new Datawarehouse();
                context.Configuration.AutoDetectChangesEnabled = false;
                // var list = Dw.DateTables.ToList();
                var list = Dw.PurchaseFacts.GroupBy(p => new {p.SupplierId,p.ProductId,p.DateId})
                    .Select(p => new
                    {
                        ProductId = p.Key.ProductId,
                        SupplierId = p.Key.SupplierId,
                        DateId = p.Key.DateId,
                        TotalCost = (int)p.Sum(s => s.Quantity * s.Product.CostPrice * (1 - s.TotalDiscount)),
                        TotalQuantity = p.Sum(s => s.Quantity),
                        TotalDiscount = (float) p.Sum(s => s.TotalDiscount),
                    });
                var i = 1;
                foreach (var userProductDateSaleFactCube in list)
                {
                    var item = new DateProductSupplierPurchaseFactCube()
                    {
                        SupplierId = userProductDateSaleFactCube.SupplierId,
                        TotalDiscount = userProductDateSaleFactCube.TotalDiscount,
                        Quantity = userProductDateSaleFactCube.TotalQuantity,
                        TotalPrice = userProductDateSaleFactCube.TotalCost,
                        ProductId = userProductDateSaleFactCube.ProductId,
                        DateId = userProductDateSaleFactCube.DateId,
                    };
                    context.DateProductSupplierPurchaseFactCubes.Add(item);
                    InvokeDgv(0, i);
                    context = Seeder.CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
                MessageBox.Show(@"Compeleted");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message + Environment.NewLine + exception.InnerException);
            }
            finally
            {
                SetButtons(true);
            }
        }
    }
}
