﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DataContext;
using DataContext.DWModel;

namespace DataExtracter
{
    public class Seeder
    {
        private const int Count = 1000;


        public static Datawarehouse CheckContext(Datawarehouse context, int i)
        {

            var db = new Datawarehouse();
            try
            {
                if (i % Count != 0) return context;
                context.SaveChanges();
                context.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + Environment.NewLine + e.InnerException);
            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = false;
            }
            return db;
        }
        public static void Products(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Products.AsNoTracking();
                foreach (var product in list)
                {
                    var p = new Product()
                    {
                        Brand = product.Brand.Name,
                        Name = product.Name,
                        SubCategory = product.SubCategory.Name,
                        Category = product.SubCategory.Category.Name,
                        Class = product.Class.Name,
                        Color = product.Color.Name,
                        CostPrice = product.CostPrice,
                        ListPrice = product.ListPrice,
                        SKU = product.SKU,
                        Size = product.Size.Name,
                        Style = product.Style.Name,
                        Weight = product.Weight,
                        SurrogateKey = product.Id
                    };
                    context.Products.Add(p);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        
        public static void CreditCards(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.CreditCards.AsNoTracking();
                foreach (var credit in list)
                {

                    var card = new CreditCard()
                    {
                        Address = credit.Address,
                        City = credit.City,
                        Country = credit.Country,
                        CreditCardNumber = credit.CreditCardNumber,
                        CreditCardType = credit.CreditCardType,
                        PostCode = credit.PostCode,
                        State = credit.State,
                        SurrogateKey = credit.Id
                    };
                    context.CreditCards.Add(card);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        public static void Shippings(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Shippings.AsNoTracking();
                foreach (var ship in list)
                {

                    var shipping = new Shipping()
                    {
                        State = ship.State,
                        Address = ship.Address,
                        City = ship.Address,
                        Country = ship.Country,
                        Name = ship.Name,
                        PostCode = ship.PostCode,
                        SurrogateKey = ship.Id
                    };
                    context.Shippings.Add(shipping);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void Invoices(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Invoices.AsNoTracking();
                foreach (var inv in list)
                {

                    var invoice = new Invoice()
                    {
                        ShippingAddress = inv.ShippingAddress,
                        ShippingCity = inv.ShippingCity,
                        ShippingCountry = inv.ShippingCountry,
                        ShippingPostCode = inv.ShippingPostCode,
                        ShippingState = inv.ShippingState,
                        SurrogateKey = inv.Id
                    };
                    context.Invoices.Add(invoice);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void UserAccounts(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.UserAccounts.AsNoTracking();
                foreach (var account in list)
                {

                    var accounts = new UserAccount()
                    {
                        SurrogateKey = account.Id,
                        PostCode = account.PostCode,
                        City = account.City,
                        Country = account.Country,
                        State = account.State,
                        Email = account.Email,
                        FullName = account.FirstName + " " + account.LastName,
                        UserName = account.UserName,
                    };
                    context.UserAccounts.Add(accounts);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void Admins(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Administrators.AsNoTracking();
                foreach (var account in list)
                {

                    var accounts = new Admin()
                    {
                        SurrogateKey = account.Id,
                        PostCode = account.PostCode,
                        City = account.City,
                        Country = account.Country,
                        State = account.State,
                        FullName = account.FirstName + " " + account.LastName,
                        UserName = account.UserName,
                    };
                    context.Adminstrator.Add(accounts);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void Supplier(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Suppliers.AsNoTracking();
                foreach (var account in list)
                {

                    var accounts = new Supplier()
                    {
                        SurrogateKey = account.Id,
                        PostCode = account.PostCode,
                        City = account.City,
                        Country = account.Country,
                        State = account.State,
                        FullName = account.FirstName + " " + account.LastName,
                        UserName = account.UserName,
                    };
                    context.Suppliers.Add(accounts);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        public static void InventoryFact(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.Inventories.AsNoTracking();
                foreach (var inventory in list)
                {

                    var idOfProduct = context.Products.First(p => p.SurrogateKey == inventory.ProductId);
                    if (idOfProduct == null) continue;
                    var dateOfInventory = inventory.DateOfTransaction;

                    var intvent = new InventoryFact()
                    {
                        ProductId = idOfProduct.Id,
                        DateId = IdOfDate(dateOfInventory),
                        QuantityAdded = inventory.QuantityAdded,
                        QuantityRemove = inventory.QuantiyRemoved,
                    };
                    context.InventoryFacts.Add(intvent);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void SaleFact(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.SaleOrderDetails.AsNoTracking();
                foreach (var sale in list)
                {

                    var idOfProduct = context.Products.FirstOrDefault(p => p.SurrogateKey == sale.ProductId);
                    var idOfCreditCard =
                        context.CreditCards.FirstOrDefault(p => p.SurrogateKey == sale.SaleOrder.CreditCardId);
                    var index = sale.SaleOrder.Id;
                    var id = dbContext.Invoices.FirstOrDefault(p => p.SaleOrder.Id == index);
                    var idOfUser =
                        context.UserAccounts.FirstOrDefault(p => p.SurrogateKey == sale.SaleOrder.CreditCard.User.Id);
                    if (idOfProduct == null || idOfCreditCard == null || id == null || idOfUser == null) continue;
                    var idOfInvoice = context.Invoices.FirstOrDefault(p => p.SurrogateKey == id.Id);
                    if (idOfInvoice == null) continue;
                    var idOfShipper = context.Shippings.FirstOrDefault(p => p.SurrogateKey == id.Shipping.Id);
                    if (idOfShipper == null) continue;
                    var dateOfSale = sale.SaleOrder.DateOfSale;
                    var idOfDate = IdOfDate(dateOfSale);
                    var salefact = new SaleFact()
                    {
                        ProductId = idOfProduct.Id,
                        CreditCardId = idOfCreditCard.Id,
                        DateId = idOfDate,
                        InvoiceId = idOfInvoice.Id,
                        Quantity = sale.Quantity,
                        TotalDiscount = sale.Discount,
                        TotalSale = (idOfProduct.ListPrice * sale.Quantity) * sale.Discount,
                        ShipperId = idOfShipper.Id,
                        UserAccountId =  idOfUser.Id,
                    };
                    context.SaleFacts.Add(salefact);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public static void PurchaseFact(int row)
        {
            var i = 1;
            try
            {
                var context = new Datawarehouse();
                var dbContext = new Database();
                context.Configuration.AutoDetectChangesEnabled = false;
                var list = dbContext.PurchaseOrderDetails.AsNoTracking();
                foreach (var purchase in list)
                {

                    var idOfProduct = context.Products.FirstOrDefault(p => p.SurrogateKey == purchase.ProductId);
                    var idOfAdmin =
                        context.Adminstrator.FirstOrDefault(
                            admin => admin.SurrogateKey == purchase.PurchaseOrder.AdminId);
                    var idOfSupplier =
                        context.Suppliers.FirstOrDefault(p => p.SurrogateKey == purchase.PurchaseOrder.SupplierId);
                    if (idOfProduct == null || idOfAdmin == null || idOfSupplier == null) continue;
                    var date = purchase.PurchaseOrder.DateOfPurchase;
                    var idOfDate = IdOfDate(date);
                    var purchasefact = new PurchaseFact()
                    {
                        AdminId = idOfAdmin.Id,
                        DateId = idOfDate,
                        ProductId = idOfProduct.Id,
                        Quantity = purchase.Quanity,
                        TotalDiscount = purchase.Discount,
                        TotalPrice = (purchase.Quanity * idOfProduct.CostPrice) * purchase.Discount,
                        SupplierId = idOfSupplier.Id,
                    };
                    context.PurchaseFacts.Add(purchasefact);
                    Form1.InvokeDgv(row, i);
                    context = CheckContext(context, i);
                    i++;
                }
                context.SaveChanges();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        private static long IdOfDate(DateTime dateOfInventory)
        {
            var context = new Datawarehouse();
            var dateId =
                        context.DateTables.FirstOrDefault(
                            d =>
                                d.Year == dateOfInventory.Year && d.Day == dateOfInventory.DayOfYear &&
                                d.Month == dateOfInventory.Month);
            if (dateId != null) return dateId.Id;
            var dateTable = new DateTable()
            {
                Date = dateOfInventory,
                Day = dateOfInventory.DayOfYear,
                DayofMonth = dateOfInventory.Day,
                Month = dateOfInventory.Month,
                Week = GetIso8601WeekOfYear(dateOfInventory),
                Year = dateOfInventory.Year,
            };
            context.DateTables.Add(dateTable);
            context.SaveChanges();
            return dateTable.Id;
        }
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            var day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

    }
}
