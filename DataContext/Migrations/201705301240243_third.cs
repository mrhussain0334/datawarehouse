namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class third : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserProductDateSaleFacts", "DateId", "dbo.DateTables");
            RenameTable(name: "dbo.UserProductDateSaleFacts", newName: "UserProductSaleFacts");
            DropIndex("dbo.UserProductSaleFacts", new[] { "DateId" });
            DropColumn("dbo.UserProductSaleFacts", "DateId");

        }
        
        public override void Down()
        {
            AddColumn("dbo.UserProductSaleFacts", "DateId", c => c.Long(nullable: false));
            CreateIndex("dbo.UserProductSaleFacts", "DateId");
            AddForeignKey("dbo.UserProductDateSaleFacts", "DateId", "dbo.DateTables", "Id", cascadeDelete: true);
            RenameTable(name: "dbo.UserProductSaleFacts", newName: "UserProductDateSaleFacts");
        }
    }
}
