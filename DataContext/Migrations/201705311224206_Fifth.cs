namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fifth : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DateProductSupplierPurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        SupplierId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId)
                .Index(t => t.DateId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.DateSupplierPurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        SupplierId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.ProductSupplierPurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        SupplierId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.SupplierPurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        SupplierId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.SupplierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SupplierPurchaseFactCubes", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.ProductSupplierPurchaseFactCubes", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.ProductSupplierPurchaseFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.DateSupplierPurchaseFactCubes", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.DateSupplierPurchaseFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.DateProductSupplierPurchaseFactCubes", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.DateProductSupplierPurchaseFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.DateProductSupplierPurchaseFactCubes", "DateId", "dbo.DateTables");
            DropIndex("dbo.SupplierPurchaseFactCubes", new[] { "SupplierId" });
            DropIndex("dbo.ProductSupplierPurchaseFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductSupplierPurchaseFactCubes", new[] { "SupplierId" });
            DropIndex("dbo.DateSupplierPurchaseFactCubes", new[] { "DateId" });
            DropIndex("dbo.DateSupplierPurchaseFactCubes", new[] { "SupplierId" });
            DropIndex("dbo.DateProductSupplierPurchaseFactCubes", new[] { "ProductId" });
            DropIndex("dbo.DateProductSupplierPurchaseFactCubes", new[] { "DateId" });
            DropIndex("dbo.DateProductSupplierPurchaseFactCubes", new[] { "SupplierId" });
            DropTable("dbo.SupplierPurchaseFactCubes");
            DropTable("dbo.ProductSupplierPurchaseFactCubes");
            DropTable("dbo.DateSupplierPurchaseFactCubes");
            DropTable("dbo.DateProductSupplierPurchaseFactCubes");
        }
    }
}
