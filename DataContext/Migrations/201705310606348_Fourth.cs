namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fourth : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProductDateSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.UserAccounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.UserId)
                .Index(t => t.DateId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserProductDateSaleFactCubes", "UserId", "dbo.UserAccounts");
            DropForeignKey("dbo.UserProductDateSaleFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.UserProductDateSaleFactCubes", "DateId", "dbo.DateTables");
            DropIndex("dbo.UserProductDateSaleFactCubes", new[] { "DateId" });
            DropIndex("dbo.UserProductDateSaleFactCubes", new[] { "UserId" });
            DropIndex("dbo.UserProductDateSaleFactCubes", new[] { "ProductId" });
            DropTable("dbo.UserProductDateSaleFactCubes");
        }
    }
}
