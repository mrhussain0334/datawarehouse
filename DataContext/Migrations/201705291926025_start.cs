namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DateInventoryFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DateId = c.Long(nullable: false),
                        QuantityAdded = c.Int(nullable: false),
                        QuantityRemoved = c.Int(nullable: false),
                        CurrentStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.DatePurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        DateId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.DateSaleFactsCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DateId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.ProductDateInventoryFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DateId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        QuantityAdded = c.Int(nullable: false),
                        QuantityRemoved = c.Int(nullable: false),
                        CurrentStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.DateId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductDatePurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        ProductId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.ProductDateSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.ProductInventoryFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        QuantityAdded = c.Int(nullable: false),
                        QuantityRemoved = c.Int(nullable: false),
                        CurrentStock = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductPurchaseFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        TotalDiscount = c.Double(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        ProductId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ProductSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ProductId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.ShippingSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ShippingId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Shippings", t => t.ShippingId, cascadeDelete: true)
                .Index(t => t.ShippingId);
            
            CreateTable(
                "dbo.UserDateSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.UserAccounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.UserProductDateSaleFacts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        ProductId = c.Long(nullable: false),
                        DateId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DateTables", t => t.DateId, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .ForeignKey("dbo.UserAccounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.ProductId)
                .Index(t => t.DateId);
            
            CreateTable(
                "dbo.UserSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserAccounts", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserSaleFactCubes", "UserId", "dbo.UserAccounts");
            DropForeignKey("dbo.UserProductDateSaleFacts", "UserId", "dbo.UserAccounts");
            DropForeignKey("dbo.UserProductDateSaleFacts", "ProductId", "dbo.Products");
            DropForeignKey("dbo.UserProductDateSaleFacts", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.UserDateSaleFactCubes", "UserId", "dbo.UserAccounts");
            DropForeignKey("dbo.UserDateSaleFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.ShippingSaleFactCubes", "ShippingId", "dbo.Shippings");
            DropForeignKey("dbo.ProductSaleFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductPurchaseFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductInventoryFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductDateSaleFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductDateSaleFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.ProductDatePurchaseFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductDatePurchaseFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.ProductDateInventoryFactCubes", "ProductId", "dbo.Products");
            DropForeignKey("dbo.ProductDateInventoryFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.DateSaleFactsCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.DatePurchaseFactCubes", "DateId", "dbo.DateTables");
            DropForeignKey("dbo.DateInventoryFactCubes", "DateId", "dbo.DateTables");
            DropIndex("dbo.UserSaleFactCubes", new[] { "UserId" });
            DropIndex("dbo.UserProductDateSaleFacts", new[] { "DateId" });
            DropIndex("dbo.UserProductDateSaleFacts", new[] { "ProductId" });
            DropIndex("dbo.UserProductDateSaleFacts", new[] { "UserId" });
            DropIndex("dbo.UserDateSaleFactCubes", new[] { "DateId" });
            DropIndex("dbo.UserDateSaleFactCubes", new[] { "UserId" });
            DropIndex("dbo.ShippingSaleFactCubes", new[] { "ShippingId" });
            DropIndex("dbo.ProductSaleFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductPurchaseFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductInventoryFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductDateSaleFactCubes", new[] { "DateId" });
            DropIndex("dbo.ProductDateSaleFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductDatePurchaseFactCubes", new[] { "DateId" });
            DropIndex("dbo.ProductDatePurchaseFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductDateInventoryFactCubes", new[] { "ProductId" });
            DropIndex("dbo.ProductDateInventoryFactCubes", new[] { "DateId" });
            DropIndex("dbo.DateSaleFactsCubes", new[] { "DateId" });
            DropIndex("dbo.DatePurchaseFactCubes", new[] { "DateId" });
            DropIndex("dbo.DateInventoryFactCubes", new[] { "DateId" });
            DropTable("dbo.UserSaleFactCubes");
            DropTable("dbo.UserProductDateSaleFacts");
            DropTable("dbo.UserDateSaleFactCubes");
            DropTable("dbo.ShippingSaleFactCubes");
            DropTable("dbo.ProductSaleFactCubes");
            DropTable("dbo.ProductPurchaseFactCubes");
            DropTable("dbo.ProductInventoryFactCubes");
            DropTable("dbo.ProductDateSaleFactCubes");
            DropTable("dbo.ProductDatePurchaseFactCubes");
            DropTable("dbo.ProductDateInventoryFactCubes");
            DropTable("dbo.DateSaleFactsCubes");
            DropTable("dbo.DatePurchaseFactCubes");
            DropTable("dbo.DateInventoryFactCubes");
        }
    }
}
