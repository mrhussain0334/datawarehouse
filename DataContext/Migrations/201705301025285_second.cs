namespace DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShippingSaleFactCubes", "ShippingId", "dbo.Shippings");
            DropIndex("dbo.ShippingSaleFactCubes", new[] { "ShippingId" });
            DropTable("dbo.ShippingSaleFactCubes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ShippingSaleFactCubes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ShippingId = c.Long(nullable: false),
                        TotalSale = c.Single(nullable: false),
                        TotalDiscount = c.Single(nullable: false),
                        TotalQuantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ShippingSaleFactCubes", "ShippingId");
            AddForeignKey("dbo.ShippingSaleFactCubes", "ShippingId", "dbo.Shippings", "Id", cascadeDelete: true);
        }
    }
}
