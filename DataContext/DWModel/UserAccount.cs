﻿using System.Collections.Generic;

namespace DataContext.DWModel
{
    public class UserAccount : Base
    {
        public long SurrogateKey { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public virtual List<SaleFact> SaleFacts { get; set; }

    }
}
