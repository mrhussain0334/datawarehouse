﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel
{
    public class Invoice : Base
    {
        public long SurrogateKey { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostCode { get; set; }
        public virtual List<SaleFact> SaleFacts { get; set; }

    }
}
