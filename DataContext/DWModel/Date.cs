﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel
{
    public class DateTable : Base
    {


        [Column(TypeName = "date")]
        public DateTime Date { get; set; }
        public int Year { get; set; }
        public int Day { get; set; }
        public int Month { get; set; }
        public int DayofMonth { get; set; }
        public int Week { get; set; }

        public virtual List<PurchaseFact> PurchaseFacts { get; set; }
        public virtual List<InventoryFact> InventoryFacts { get; set; }

        public virtual List<SaleFact> SaleFacts { get; set; }

    }
}
