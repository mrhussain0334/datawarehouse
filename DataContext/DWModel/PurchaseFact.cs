﻿using System.ComponentModel.DataAnnotations.Schema;
namespace DataContext.DWModel
{
    public class PurchaseFact : Base
    {
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }
        public long AdminId { get; set; }
        [ForeignKey("AdminId")]
        public virtual Admin Admin { get; set; }
        public long SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }
        public int Quantity { get; set; }
        public double TotalDiscount { get; set; }
        public double TotalPrice { get; set; }
    }
}
