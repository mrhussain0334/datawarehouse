﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel
{
    public class Admin : Base
    {
        public long SurrogateKey { get; set; }
        public string UserName { get; set; }

        public string FullName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public virtual List<PurchaseFact> PurchaseFacts { get; set; }
    }
}
