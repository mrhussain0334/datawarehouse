﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel
{
    public class CreditCard : Base
    {
        public long SurrogateKey { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardType { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public virtual List<SaleFact> SaleFacts { get; set; }


    }
}
