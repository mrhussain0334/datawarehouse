﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel.InventoryFactCube
{
    public class DateInventoryFactCube : Base
    {
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }

        public int QuantityAdded { get; set; }
        public int QuantityRemoved { get; set; }
        public int CurrentStock { get; set; }

    }
}
