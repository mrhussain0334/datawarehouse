﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.InventoryFactCube
{
    public class ProductInventoryFactCube : Base
    {

        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public int QuantityAdded { get; set; }
        public int QuantityRemoved { get; set; }
        public int CurrentStock { get; set; }
    }
}
