﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel
{
    public class SaleFact : Base
    {


        public long InvoiceId { get; set; }
        [ForeignKey("InvoiceId")]
        public virtual Invoice Invoice { get; set; }
        public long ShipperId { get; set; }
        public virtual Shipping Shipping { get; set; }
        public long ProductId  { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }


        public long UserAccountId { get; set; }
        [ForeignKey("UserAccountId")]
        public virtual UserAccount UserAccount { get; set; }

        public long CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual CreditCard CreditCard { get; set; }

        public int Quantity { get; set; }
        public double TotalSale { get; set; }
        public double TotalDiscount { get; set; }

    }
}
