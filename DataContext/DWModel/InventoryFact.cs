﻿using System.ComponentModel.DataAnnotations.Schema;
namespace DataContext.DWModel
{
    public class InventoryFact : Base
    {
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }

        public int QuantityAdded { get; set; }
        public int QuantityRemove { get; set; }

    }
}
