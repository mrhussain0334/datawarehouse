﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.PurchaseFactCube
{
    public class DatePurchaseFactCube : Base
    {
        public int Quantity { get; set; }
        public double TotalDiscount { get; set; }
        public double TotalPrice { get; set; }
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }

    }
}
