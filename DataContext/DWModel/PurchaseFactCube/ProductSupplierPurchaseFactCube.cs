﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.PurchaseFactCube
{
    public class ProductSupplierPurchaseFactCube : Base
    {
        public int Quantity { get; set; }
        public double TotalDiscount { get; set; }
        public double TotalPrice { get; set; }
        public long SupplierId { get; set; }
        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}
