﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.SaleFactCube
{
    public class UserSaleFactCube : Base
    {
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserAccount UserAccount { get; set; }
        public float TotalSale { get; set; }
        public float TotalDiscount { get; set; }
        public int TotalQuantity { get; set; }
    }
}
