﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel.SaleFactCube
{
    public class ProductDateSaleFactCube : Base
    {
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }
        public float TotalSale { get; set; }
        public float TotalDiscount { get; set; }
        public int TotalQuantity { get; set; }
    }
}
