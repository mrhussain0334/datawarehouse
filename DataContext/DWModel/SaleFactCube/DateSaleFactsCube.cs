﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.SaleFactCube
{
    public class DateSaleFactsCube : Base
    {
        public long DateId { get; set; }
        [ForeignKey("DateId")]
        public virtual DateTable DateTable { get; set; }
        public float TotalSale { get; set; }
        public float TotalDiscount { get; set; }
        public int TotalQuantity { get; set; }
    }
}
