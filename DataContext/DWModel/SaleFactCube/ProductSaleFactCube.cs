﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.DWModel.SaleFactCube
{
    public class ProductSaleFactCube : Base
    {
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public float TotalSale { get; set; }
        public float TotalDiscount { get; set; }
        public int TotalQuantity { get; set; }
    }
}
