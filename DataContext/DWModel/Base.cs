﻿using System.ComponentModel.DataAnnotations;

namespace DataContext.DWModel
{
    public class Base
    {
        [Key]
        public long Id { get; set; }
    }
}
