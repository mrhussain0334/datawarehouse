﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.DWModel
{
    public class Product : Base
    {

        public long SurrogateKey { get; set; }
        public string Name { get; set; }
        public string SKU { get; set; }
        public float Weight { get; set; }
        public float ListPrice { get; set; }
        public float CostPrice { get; set; }
        public string Brand { get; set; }
        public string Class { get; set; }
        public string Color { get; set; }
        public string Style { get; set; }
        public string Size { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
        public virtual List<PurchaseFact> PurchaseFacts { get; set; }

        public virtual List<InventoryFact> InventoryFacts { get; set; }
        public virtual List<SaleFact> SaleFacts { get; set; }
    }
}
