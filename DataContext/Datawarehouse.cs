﻿using System.Data.Entity;
using DataContext.DWModel;
using DataContext.DWModel.InventoryFactCube;
using DataContext.DWModel.PurchaseFactCube;
using DataContext.DWModel.SaleFactCube;

namespace DataContext
{
    public class Datawarehouse : DbContext
    {
        public Datawarehouse() : base("OLAP")
        {

        }
        public DbSet<Admin> Adminstrator { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<DateTable> DateTables { get; set; }
        public DbSet<InventoryFact> InventoryFacts { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PurchaseFact> PurchaseFacts { get; set; }
        public DbSet<SaleFact> SaleFacts { get; set; }
        public DbSet<Shipping> Shippings { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }

        #region SaleFact
        public DbSet<DateSaleFactsCube> DateSaleFactsCubes { get; set; }
        public DbSet<ProductSaleFactCube> ProductSaleFactCubes { get; set; }
        public DbSet<UserSaleFactCube> UserSaleFactCubes { get; set; }
        public DbSet<ProductDateSaleFactCube> ProductDateSaleFactCubes { get; set; }
        public DbSet<UserDateSaleFactCube> UserDateSaleFactCubes { get; set; }
        public DbSet<UserProductSaleFact> UserProductSaleFacts { get; set; }
        public DbSet<UserProductDateSaleFactCube> UserProductDateSaleFactCubes { get; set; }

        #endregion

        #region PurchaseFactCube

        public DbSet<DatePurchaseFactCube> DatePurchaseFactCubes { get; set; }
        public DbSet<ProductDatePurchaseFactCube> ProductDatePurchaseFactCubes { get; set; }
        public DbSet<ProductPurchaseFactCube> ProductPurchaseFactCubes { get; set; }
        public DbSet<ProductSupplierPurchaseFactCube> ProductSupplierPurchaseFactCubes { get; set; }
        public DbSet<SupplierPurchaseFactCube> SupplierPurchaseFactCubes { get; set; }
        public DbSet<DateSupplierPurchaseFactCube> DateSupplierPurchaseFactCubes { get; set; }
        public DbSet<DateProductSupplierPurchaseFactCube> DateProductSupplierPurchaseFactCubes { get; set; }

        #endregion

        #region InventoryFactCube

        public DbSet<DateInventoryFactCube> DateInventoryFactCubes { get; set; }
        public DbSet<ProductDateInventoryFactCube> ProductDateInventoryFactCubes { get; set; }
        public DbSet<ProductInventoryFactCube> ProductInventoryFactCubes { get; set; }


        #endregion
    }
}
