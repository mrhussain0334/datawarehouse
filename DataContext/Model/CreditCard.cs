﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class CreditCard : EntityBase
    {

        public string CreditCardNumber { get; set; }
        public string CreditCardType { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }

        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserAccount User { get; set; }
        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }
    }

}
