﻿using System.ComponentModel.DataAnnotations;

namespace DataContext.Model
{
    public class EntityBase
    {
        [Key]
        public long Id { get; set; }
    }
}
