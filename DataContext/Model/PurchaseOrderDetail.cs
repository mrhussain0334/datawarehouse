﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class PurchaseOrderDetail : EntityBase
    {
        public int Quanity { get; set; }
        public float Discount { get; set; }
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public long? PurchaseOrderId { get; set; }
        [ForeignKey("PurchaseOrderId")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }
    }
}
