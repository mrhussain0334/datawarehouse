﻿using System;
using System.Collections.Generic;

namespace DataContext.Model
{
    public class UserAccount : EntityBase
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public DateTime DateRegistered { get; set; }
        public string Email { get; set; }
        public virtual List<CreditCard> CreditCards { get; set; }
    }
}
