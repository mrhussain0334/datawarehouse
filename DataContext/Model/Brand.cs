﻿using System.Collections.Generic;

namespace DataContext.Model
{
    public class Brand : EntityBase
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
