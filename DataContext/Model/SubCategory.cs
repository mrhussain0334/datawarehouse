﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class SubCategory : EntityBase
    {
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
