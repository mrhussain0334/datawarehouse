﻿using System;
using System.Collections.Generic;

namespace DataContext.Model
{
    public class Category : EntityBase
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }


        public virtual List<SubCategory> SubCategories { get; set; }
    }
}
