﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class PurchaseOrder : EntityBase
    {
        public long SupplierId { get; set; }
        public DateTime DateOfPurchase { get; set; }
        public long AdminId { get; set; }
        [ForeignKey("SupplierId")]
        public virtual Supplier Supplier { get; set; }
        [ForeignKey("AdminId")]
        public virtual Administrator Administrator { get; set; }
        public virtual List<PurchaseOrderDetail> PurchaseOrderDetails { get; set; }
        public virtual List<Inventory> Inventories { get; set; }
    }
}
