﻿using System.Collections.Generic;

namespace DataContext.Model
{
    public class Class : EntityBase
    {
        public string Name { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
