﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataContext.Model
{
    public class SaleOrder : EntityBase
    {
        public DateTime DateOfSale { get; set; }
        public long CreditCardId { get; set; }
        [ForeignKey("CreditCardId")]
        public virtual CreditCard CreditCard { get; set; }

        public virtual List<SaleOrderDetail> SaleOrderDetails { get; set; }
        public virtual List<Inventory> Inventories { get; set; }
    }
}
