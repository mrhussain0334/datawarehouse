﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Invoice : EntityBase
    {
        public long SaleOrderId { get; set; }
        [ForeignKey("SaleOrderId")]
        public virtual SaleOrder SaleOrder { get; set; }
        public long ShippingId { get; set; }
        [ForeignKey("ShippingId")]
        public virtual Shipping Shipping { get; set; }
        public float FreightCost { get; set; }
        public float ShippingCost { get; set; }
        public float Discount { get; set; }
        public string ShippingAddress { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostCode { get; set; }

    }
}
