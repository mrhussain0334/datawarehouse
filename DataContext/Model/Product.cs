﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Product : EntityBase
    {
        public string Name { get; set; }
        public string SKU { get; set; }

        public long BrandId { get; set; }
        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }

        /// <summary>
        /// Color of your Name
        /// </summary>
        public long ColorId { get; set; }
        [ForeignKey("ColorId")]
        public virtual Color Color { get; set; }
        /// <summary>
        /// Large / Small / Medium / XLarge
        /// </summary>
        public long SizeId { get; set; }
        [ForeignKey("SizeId")]
        public virtual Size Size { get; set; }
        /// <summary>
        /// M or F or U
        /// </summary>
        public long StyleId { get; set; }
        [ForeignKey("StyleId")]
        public virtual Style Style { get; set; }
        /// <summary>
        /// Low High Medium
        /// </summary>
        public long ClassId { get; set; }
        [ForeignKey("ClassId")]
        public virtual Class Class { get; set; }
        public float Weight { get; set; }
        public float ListPrice { get; set; }
        public float CostPrice { get; set; }
        public long AdminId { get; set; }
        [ForeignKey("AdminId")]
        public virtual Administrator Admin { get; set; }
        public long SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategory SubCategory { get; set; }
    }
}
