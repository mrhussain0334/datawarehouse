﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Account : EntityBase
    {
        public string AccountHolderName { get; set; }
        public string AccountNumber { get; set; }
        public DateTime DateofCreation { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankCity { get; set; }
        public string BankPostCode { get; set; }
        public string BankCountry { get; set; }
        public long? AdminId { get; set; }
        [ForeignKey("AdminId")]
        public virtual Administrator Administrator { get; set; }
    }
}
