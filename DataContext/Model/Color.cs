﻿namespace DataContext.Model
{
    public class Color : EntityBase
    {
        public string Name { get; set; }
        public string Argb { get; set; }

    }
}
