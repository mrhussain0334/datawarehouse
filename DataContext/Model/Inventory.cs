﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class Inventory : EntityBase
    {
        public long ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [DefaultValue(0)]
        public int QuantityAdded { get; set; }
        [DefaultValue(0)]
        public int QuantiyRemoved { get; set; }
        public DateTime DateOfTransaction { get; set; }

        public long? PurchaseOrderId { get; set; }
        [ForeignKey("PurchaseOrderId")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }

        public long? SaleOrderId { get; set; }
        [ForeignKey("SaleOrderId")]
        public virtual SaleOrder SaleOrder { get; set; }
        
    }
}
