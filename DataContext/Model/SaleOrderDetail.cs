﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataContext.Model
{
    public class SaleOrderDetail : EntityBase
    {
        public long? SaleOrderId { get; set; }
        [ForeignKey("SaleOrderId")]
        public virtual SaleOrder SaleOrder { get; set; }
        public long ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
        public float Discount { get; set; }
    }
}
