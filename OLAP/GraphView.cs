﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace OLAP
{
    public partial class GraphView : Form
    {
        private DataTable dt;
        public GraphView(DataTable dt)
        {
            this.dt = dt;
            InitializeComponent();
        }

        private void GraphView_Load(object sender, EventArgs e)
        {
            for (var i = 0; i < dt.Rows.Count; i++)
            {
                chart1.Series[0].Points.AddXY(dt.Rows[i][0].ToString(), dt.Rows[i][dt.Columns.Count - 1].ToString());
            }
            var chartArea = chart1.ChartAreas[0];
            // set view range to [0,max]
            chartArea.AxisX.Minimum = 0;
            chartArea.AxisX.Maximum = dt.Rows.Count + 1;

            // enable autoscroll
            chartArea.CursorX.AutoScroll = true;

            // let's zoom to [0,blockSize] (e.g. [0,100])
            chartArea.AxisX.ScaleView.Zoomable = true;
            chartArea.AxisX.Interval = 1;
            var position = 0;
            var size = 100;
            chartArea.AxisX.ScaleView.Zoom(position, size);

            // disable zoom-reset button (only scrollbar's arrows are available)
            chartArea.AxisX.ScrollBar.ButtonStyle = ScrollBarButtonStyles.SmallScroll;

            // set scrollbar small change to blockSize (e.g. 100)
            chartArea.AxisX.ScaleView.SmallScrollSize = size;
            chart1.Refresh();
            chart1.ResetAutoValues();
        }
    }
}
