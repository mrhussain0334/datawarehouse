﻿namespace OLAP
{
    partial class OlapDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaleFact = new System.Windows.Forms.Button();
            this.btnInventoryFact = new System.Windows.Forms.Button();
            this.btnPurchaseFact = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSaleFact
            // 
            this.btnSaleFact.Location = new System.Drawing.Point(13, 13);
            this.btnSaleFact.Name = "btnSaleFact";
            this.btnSaleFact.Size = new System.Drawing.Size(160, 23);
            this.btnSaleFact.TabIndex = 0;
            this.btnSaleFact.Text = "Sale Facts";
            this.btnSaleFact.UseVisualStyleBackColor = true;
            this.btnSaleFact.Click += new System.EventHandler(this.btnSaleFact_Click);
            // 
            // btnInventoryFact
            // 
            this.btnInventoryFact.Location = new System.Drawing.Point(13, 59);
            this.btnInventoryFact.Name = "btnInventoryFact";
            this.btnInventoryFact.Size = new System.Drawing.Size(160, 23);
            this.btnInventoryFact.TabIndex = 1;
            this.btnInventoryFact.Text = "Inventory Facts";
            this.btnInventoryFact.UseVisualStyleBackColor = true;
            this.btnInventoryFact.Click += new System.EventHandler(this.btnInventoryFact_Click);
            // 
            // btnPurchaseFact
            // 
            this.btnPurchaseFact.Location = new System.Drawing.Point(13, 117);
            this.btnPurchaseFact.Name = "btnPurchaseFact";
            this.btnPurchaseFact.Size = new System.Drawing.Size(160, 23);
            this.btnPurchaseFact.TabIndex = 2;
            this.btnPurchaseFact.Text = "Purchase Facts";
            this.btnPurchaseFact.UseVisualStyleBackColor = true;
            this.btnPurchaseFact.Click += new System.EventHandler(this.btnPurchaseFact_Click);
            // 
            // OlapDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 261);
            this.Controls.Add(this.btnPurchaseFact);
            this.Controls.Add(this.btnInventoryFact);
            this.Controls.Add(this.btnSaleFact);
            this.Name = "OlapDashboard";
            this.Text = "OlapDashboard";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSaleFact;
        private System.Windows.Forms.Button btnInventoryFact;
        private System.Windows.Forms.Button btnPurchaseFact;
    }
}