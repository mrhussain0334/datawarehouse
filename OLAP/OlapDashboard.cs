﻿using System;
using System.Windows.Forms;

namespace OLAP
{
    public partial class OlapDashboard : Form
    {
        public OlapDashboard()
        {
            InitializeComponent();
        }

        private void btnSaleFact_Click(object sender, EventArgs e)
        {
            var fact = new SalesFactDashbaord();
            fact.ShowDialog();
        }

        private void btnInventoryFact_Click(object sender, EventArgs e)
        {
            var fact = new InventoryFactDashboard();
            fact.ShowDialog();
        }

        private void btnPurchaseFact_Click(object sender, EventArgs e)
        {
            var fact = new PurchaseFactDashboard();
            fact.ShowDialog();
        }
    }
}
