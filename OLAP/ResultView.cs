﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OLAP
{
    public partial class ResultView : Form
    {
        private DataTable dt;
        public ResultView(DataTable dt)
        {
            this.dt = dt;
            InitializeComponent();
        }

        private void ResultView_Load(object sender, EventArgs e)
        {
            dgvResult.DataSource = dt;
            dgvResult.SortCompare += DgvResult_SortCompare;
            dgvResult.ColumnHeaderMouseClick += DgvResult_ColumnHeaderMouseClick;
        }

        private void DgvResult_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var col = dgvResult.Columns[e.ColumnIndex];
            var contains = col.Name.IndexOf("total", StringComparison.OrdinalIgnoreCase) >= 0;
            if (contains)
            {
                //dgvResult.Sort(col,ListSortDirection.Descending);
            }
            else
            {
                
            }
        }

        private void DgvResult_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            
        }

        private void btnChart_Click(object sender, EventArgs e)
        {
            var chartView = new GraphView(dt);
            chartView.ShowDialog();
        }

        private int StringCompare(string a,string b)
        {
            return -string.Compare(a, b, StringComparison.OrdinalIgnoreCase);
        }
        private int IntCompare(int a,int b)
        {
            return Math.Sign(a.CompareTo(b));
        }
        private void btnGraph_Click(object sender, EventArgs e)
        {
            var chartView = new GraphView(dt);
            chartView.ShowDialog();
        }
    }
}
