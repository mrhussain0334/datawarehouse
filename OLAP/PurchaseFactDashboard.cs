﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using DataContext;
using OLAP.View;

namespace OLAP
{
    public partial class PurchaseFactDashboard : Form
    {
        public PurchaseFactDashboard()
        {
            InitializeComponent();
            Initialize();
        }
        private void Initialize()
        {
            cbProduct.Checked = false;
            cbDate.Checked = false;
            cbSupplier.Checked = false;
            rbProductAll.Checked = true;
            rbDateAll.Checked = true;
            rbSupplierAll.Checked = true;
            cbxDateWise.SelectedIndex = 0;
            txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
            txtSupplierSearch.Enabled = rbSupplierAll.Enabled = rbProductSearch.Enabled = false;

        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            var result = new List<PurchaseView>();
            var dt = new DataTable("Result");
            if (cbDate.Checked || cbProduct.Checked || cbSupplier.Checked)
            {
                if (cbDate.Checked && cbProduct.Checked && cbSupplier.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var supplierFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    if (!rbSupplierAll.Checked)
                    {
                        supplierFilter = txtSupplierSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.DateProductSupplierPurchaseFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter) &&
                                p.Supplier.FullName.Contains(supplierFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate));
                    dt.Columns.Add("Product Name", typeof(string));
                    dt.Columns.Add("Supplier Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year");

                        result = list.GroupBy(p => new { p.ProductId,p.SupplierId, p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Year + "",
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Month,p.DateTable.Year,p.SupplierId }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId,p.SupplierId, p.DateTable.Week,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new PurchaseView()
                        {
                            DateTime = p.DateTable.Date.ToString(CultureInfo.InvariantCulture),
                            SortDateTime = p.DateTable.Date,
                            Product = p.Product.Name,
                            TotalDiscount = (float)p.TotalDiscount,
                            SupplierName = p.Supplier.FullName,
                            TotalCost = (float)p.TotalPrice,
                            TotalQuantity = p.Quantity
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product,sale.SupplierName, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbDate.Checked && cbProduct.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.ProductDatePurchaseFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate));
                    dt.Columns.Add("Product Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year",typeof(int));

                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Year + "",
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float) p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float) p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Month,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Week,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new PurchaseView()
                        {
                            DateTime = p.DateTable.Date.ToString(CultureInfo.InvariantCulture),
                            SortDateTime = p.DateTable.Date,
                            Product = p.Product.Name,
                            TotalDiscount = (float) p.TotalDiscount,
                            TotalCost = (float) p.TotalPrice,
                            TotalQuantity = p.Quantity
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbDate.Checked && cbSupplier.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbSupplierAll.Checked)
                    {
                        userFilter = txtSupplierSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.DateProductSupplierPurchaseFactCubes.Where(
                            p =>
                                p.Supplier.FullName.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate));
                    dt.Columns.Add("Supplier Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year");

                        result = list.GroupBy(p => new { p.SupplierId, p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Year + "",
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.SupplierId, p.DateTable.Month,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.SupplierId, p.DateTable.Week,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            SupplierName = p.FirstOrDefault().Supplier.FullName,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new PurchaseView()
                        {
                            DateTime = p.DateTable.Date.ToString(CultureInfo.InvariantCulture),
                            SortDateTime = p.DateTable.Date,
                            SupplierName = p.Supplier.FullName,
                            TotalDiscount = (float)p.TotalDiscount,
                            TotalCost = (float)p.TotalPrice,
                            TotalQuantity = p.Quantity
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.SupplierName, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbProduct.Checked && cbSupplier.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var supplierFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    if (!rbSupplierAll.Checked)
                    {
                        supplierFilter = txtSupplierSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.DateProductSupplierPurchaseFactCubes.Where(
                            p =>
                                p.Supplier.FullName.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate));
                    result = list.Select(p => new PurchaseView()
                    {
                        SupplierName = p.Supplier.FullName,
                        Product = p.Product.Name,
                        TotalCost = (float) p.TotalPrice,
                        TotalDiscount = (float) p.TotalDiscount,
                        TotalQuantity = p.Quantity,
                    }).ToList();
                    dt.Columns.Add("Product Name", typeof(string));
                    dt.Columns.Add("Supplier Name", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.SupplierName, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbDate.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.DatePurchaseFactCubes.Where(
                            p =>
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new { p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Date.Year + "",
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Date.Year,
                        }).OrderBy(p => (p.Sort)).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.DateTable.Month,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => (p.Sort)).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.DateTable.Week,p.DateTable.Year }).Select(p => new PurchaseView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            TotalQuantity = p.Sum(asc => asc.Quantity),
                            TotalDiscount = (float)p.Sum(asc => asc.TotalDiscount),
                            TotalCost = (float)p.Sum(asc => asc.TotalPrice),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => (p.Sort)).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new PurchaseView()
                        {
                            DateTime = p.DateTable.Date.ToString(CultureInfo.InvariantCulture),
                            SortDateTime = p.DateTable.Date,
                            TotalDiscount = (float)p.TotalDiscount,
                            TotalCost = (float)p.TotalPrice,
                            TotalQuantity = p.Quantity
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbProduct.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.ProductPurchaseFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter)).ToList();
                    result = list.Select(p => new PurchaseView()
                    {
                        Product = p.Product.Name,
                        TotalDiscount = (float)p.TotalDiscount,
                        TotalCost = (float)p.TotalPrice,
                        TotalQuantity = p.Quantity
                    }).OrderBy(p => p.Product).ToList();
                    dt.Columns.Add("Product Name", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                else if (cbSupplier.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    if (!rbSupplierAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.SupplierPurchaseFactCubes.Where(
                            p =>
                                p.Supplier.FullName.Contains(userFilter)).ToList();
                    result = list.Select(p => new PurchaseView()
                    {
                        SupplierName = p.Supplier.FullName,
                        TotalDiscount = (float)p.TotalDiscount,
                        TotalCost = (float)p.TotalPrice,
                        TotalQuantity = p.Quantity
                    }).OrderBy(p => p.Product).ToList();
                    dt.Columns.Add("Supplier Name", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Cost", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.SupplierName, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalCost);
                    }
                }
                var resultform = new ResultView(dt);
                resultform.ShowDialog();
            }
            else
            {
                MessageBox.Show(@"Select Any Dimensions");
            }
        }

        private void cbProduct_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbProduct.Checked)
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            }
            else
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = true;
            }
        }

        private void cbDate_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbDate.Checked)
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
            }
            else
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = true;
            }
        }

        private void cbSupplier_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbSupplier.Checked)
            {
                txtSupplierSearch.Enabled = rbSupplierAll.Enabled = rbProductSearch.Enabled = false;
            }
            else
            {
                txtSupplierSearch.Enabled = rbSupplierAll.Enabled = rbProductSearch.Enabled = true;
            }
        }
    }
}
