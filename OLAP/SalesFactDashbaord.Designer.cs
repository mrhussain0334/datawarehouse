﻿namespace OLAP
{
    partial class SalesFactDashbaord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtUserSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductSearch = new System.Windows.Forms.TextBox();
            this.rbUserSearch = new System.Windows.Forms.RadioButton();
            this.rbUserAll = new System.Windows.Forms.RadioButton();
            this.rbProductAll = new System.Windows.Forms.RadioButton();
            this.rbProductSearch = new System.Windows.Forms.RadioButton();
            this.rbDateAll = new System.Windows.Forms.RadioButton();
            this.rbDateSearch = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbxDateWise = new System.Windows.Forms.ComboBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cbUser = new System.Windows.Forms.CheckBox();
            this.cbProduct = new System.Windows.Forms.CheckBox();
            this.cbDate = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUserSearch
            // 
            this.txtUserSearch.Location = new System.Drawing.Point(14, 35);
            this.txtUserSearch.Name = "txtUserSearch";
            this.txtUserSearch.Size = new System.Drawing.Size(293, 20);
            this.txtUserSearch.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "User Search";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Product Search";
            // 
            // txtProductSearch
            // 
            this.txtProductSearch.Location = new System.Drawing.Point(14, 115);
            this.txtProductSearch.Name = "txtProductSearch";
            this.txtProductSearch.Size = new System.Drawing.Size(293, 20);
            this.txtProductSearch.TabIndex = 4;
            // 
            // rbUserSearch
            // 
            this.rbUserSearch.AutoSize = true;
            this.rbUserSearch.Location = new System.Drawing.Point(6, 23);
            this.rbUserSearch.Name = "rbUserSearch";
            this.rbUserSearch.Size = new System.Drawing.Size(122, 17);
            this.rbUserSearch.TabIndex = 8;
            this.rbUserSearch.TabStop = true;
            this.rbUserSearch.Text = "Include User Search";
            this.rbUserSearch.UseVisualStyleBackColor = true;
            // 
            // rbUserAll
            // 
            this.rbUserAll.AutoSize = true;
            this.rbUserAll.Location = new System.Drawing.Point(6, 46);
            this.rbUserAll.Name = "rbUserAll";
            this.rbUserAll.Size = new System.Drawing.Size(104, 17);
            this.rbUserAll.TabIndex = 9;
            this.rbUserAll.TabStop = true;
            this.rbUserAll.Text = "Include All Users";
            this.rbUserAll.UseVisualStyleBackColor = true;
            // 
            // rbProductAll
            // 
            this.rbProductAll.AutoSize = true;
            this.rbProductAll.Location = new System.Drawing.Point(7, 42);
            this.rbProductAll.Name = "rbProductAll";
            this.rbProductAll.Size = new System.Drawing.Size(114, 17);
            this.rbProductAll.TabIndex = 11;
            this.rbProductAll.TabStop = true;
            this.rbProductAll.Text = "Include All Product";
            this.rbProductAll.UseVisualStyleBackColor = true;
            // 
            // rbProductSearch
            // 
            this.rbProductSearch.AutoSize = true;
            this.rbProductSearch.Location = new System.Drawing.Point(7, 19);
            this.rbProductSearch.Name = "rbProductSearch";
            this.rbProductSearch.Size = new System.Drawing.Size(137, 17);
            this.rbProductSearch.TabIndex = 10;
            this.rbProductSearch.TabStop = true;
            this.rbProductSearch.Text = "Include Product Search";
            this.rbProductSearch.UseVisualStyleBackColor = true;
            // 
            // rbDateAll
            // 
            this.rbDateAll.AutoSize = true;
            this.rbDateAll.Location = new System.Drawing.Point(6, 40);
            this.rbDateAll.Name = "rbDateAll";
            this.rbDateAll.Size = new System.Drawing.Size(100, 17);
            this.rbDateAll.TabIndex = 15;
            this.rbDateAll.TabStop = true;
            this.rbDateAll.Text = "Include All Date";
            this.rbDateAll.UseVisualStyleBackColor = true;
            // 
            // rbDateSearch
            // 
            this.rbDateSearch.AutoSize = true;
            this.rbDateSearch.Location = new System.Drawing.Point(6, 20);
            this.rbDateSearch.Name = "rbDateSearch";
            this.rbDateSearch.Size = new System.Drawing.Size(121, 17);
            this.rbDateSearch.TabIndex = 14;
            this.rbDateSearch.TabStop = true;
            this.rbDateSearch.Text = "Include Date Range";
            this.rbDateSearch.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Date Range";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(12, 194);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpFrom.TabIndex = 16;
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(12, 242);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 20);
            this.dtpTo.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "From";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "To";
            // 
            // cbxDateWise
            // 
            this.cbxDateWise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDateWise.FormattingEnabled = true;
            this.cbxDateWise.Items.AddRange(new object[] {
            "Select Date Wise",
            "Year Wise",
            "Month Wise",
            "Week Wise",
            "Day Wise"});
            this.cbxDateWise.Location = new System.Drawing.Point(357, 194);
            this.cbxDateWise.Name = "cbxDateWise";
            this.cbxDateWise.Size = new System.Drawing.Size(206, 21);
            this.cbxDateWise.TabIndex = 20;
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(13, 296);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(124, 23);
            this.btnProcess.TabIndex = 21;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.button1_Click);
            // 
            // cbUser
            // 
            this.cbUser.AutoSize = true;
            this.cbUser.Location = new System.Drawing.Point(459, 35);
            this.cbUser.Name = "cbUser";
            this.cbUser.Size = new System.Drawing.Size(105, 17);
            this.cbUser.TabIndex = 23;
            this.cbUser.Text = "Users Dimension";
            this.cbUser.UseVisualStyleBackColor = true;
            this.cbUser.CheckedChanged += new System.EventHandler(this.cbUser_CheckedChanged);
            // 
            // cbProduct
            // 
            this.cbProduct.AutoSize = true;
            this.cbProduct.Location = new System.Drawing.Point(459, 115);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(118, 17);
            this.cbProduct.TabIndex = 24;
            this.cbProduct.Text = "Product  Dimension";
            this.cbProduct.UseVisualStyleBackColor = true;
            this.cbProduct.CheckedChanged += new System.EventHandler(this.cbProduct_CheckedChanged);
            // 
            // cbDate
            // 
            this.cbDate.AutoSize = true;
            this.cbDate.Location = new System.Drawing.Point(357, 227);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(104, 17);
            this.cbDate.TabIndex = 25;
            this.cbDate.Text = "Date  Dimension";
            this.cbDate.UseVisualStyleBackColor = true;
            this.cbDate.CheckedChanged += new System.EventHandler(this.cbDate_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbUserSearch);
            this.groupBox1.Controls.Add(this.rbUserAll);
            this.groupBox1.Location = new System.Drawing.Point(313, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(140, 70);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Users";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbProductSearch);
            this.groupBox2.Controls.Add(this.rbProductAll);
            this.groupBox2.Location = new System.Drawing.Point(313, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(140, 71);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Products";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbDateSearch);
            this.groupBox3.Controls.Add(this.rbDateAll);
            this.groupBox3.Location = new System.Drawing.Point(218, 176);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(124, 68);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Date";
            // 
            // SalesFactDashbaord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 390);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.cbProduct);
            this.Controls.Add(this.cbUser);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.cbxDateWise);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProductSearch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtUserSearch);
            this.Name = "SalesFactDashbaord";
            this.Text = "SalesFactDashbaord";
            this.Load += new System.EventHandler(this.SalesFactDashbaord_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtUserSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductSearch;
        private System.Windows.Forms.RadioButton rbUserSearch;
        private System.Windows.Forms.RadioButton rbUserAll;
        private System.Windows.Forms.RadioButton rbProductAll;
        private System.Windows.Forms.RadioButton rbProductSearch;
        private System.Windows.Forms.RadioButton rbDateAll;
        private System.Windows.Forms.RadioButton rbDateSearch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbxDateWise;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.CheckBox cbUser;
        private System.Windows.Forms.CheckBox cbProduct;
        private System.Windows.Forms.CheckBox cbDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}