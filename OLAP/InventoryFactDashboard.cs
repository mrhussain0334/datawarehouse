﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataContext;
using OLAP.View;

namespace OLAP
{
    public partial class InventoryFactDashboard : Form
    {
        public InventoryFactDashboard()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            cbProduct.Checked = false;
            rbProductAll.Checked = true;
            rbDateAll.Checked = true;
            cbxDateWise.SelectedIndex = 0;
            txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
        }
        private void InventoryFactDashboard_Load(object sender, EventArgs e)
        {

        }

        private void cbProduct_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbProduct.Checked)
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            }
            else
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = true;
            }
        }

        private void cbDate_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbDate.Checked)
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
            }
            else
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = true;
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            var result = new List<InventoryView>();
            var dt = new DataTable("Result");
            if (cbDate.Checked || cbProduct.Checked)
            {
                
                if (cbDate.Checked && cbProduct.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.ProductDateInventoryFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate));
                    dt.Columns.Add("Product Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year");

                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Year + "",
                            Product = p.FirstOrDefault().Product.Name,
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Month,p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Week,p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new InventoryView()
                        {
                            DateTime = p.DateTable.Date.ToString(CultureInfo.InvariantCulture),
                            SortDateTime = p.DateTable.Date,
                            Product = p.Product.Name,
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Quantity Added", typeof(int));
                    dt.Columns.Add("Quantity Removed", typeof(int));
                    dt.Columns.Add("Total Remaining", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.DateTime, sale.QuantityAdded,
                            sale.QuantityRemoved, sale.CurrentStock);
                    }
                }
                else if (cbDate.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.DateInventoryFactCubes.Where(
                            p =>
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new { p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Date.Year + "",
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Date.Year,
                        }).OrderBy(p =>(p.Sort)).ToList();
                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.DateTable.Month,p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => (p.Sort)).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(int));
                        result = list.GroupBy(p => new { p.DateTable.Week,p.DateTable.Year }).Select(p => new InventoryView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            QuantityAdded = p.Sum(asc => asc.QuantityAdded),
                            QuantityRemoved = p.Sum(asc => asc.QuantityRemoved),
                            CurrentStock = p.Sum(asc => asc.CurrentStock),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => (p.Sort)).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new InventoryView()
                        {
                            DateTime = p.DateTable.Date.ToShortDateString(),
                            SortDateTime = p.DateTable.Date,
                            QuantityAdded = p.QuantityAdded,
                            QuantityRemoved = p.QuantityRemoved,
                            CurrentStock = p.CurrentStock,
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Quantity Added", typeof(int));
                    dt.Columns.Add("Quantity Removed", typeof(int));
                    dt.Columns.Add("Total Remaining", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.DateTime, sale.QuantityAdded,
                            sale.QuantityRemoved, sale.CurrentStock);
                    }
                }
                else if (cbProduct.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.ProductInventoryFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter)).ToList();
                    result = list.Select(p => new InventoryView()
                    {
                        Product = p.Product.Name,
                        QuantityAdded = p.QuantityAdded,
                        QuantityRemoved = p.QuantityRemoved,
                        CurrentStock = p.CurrentStock,
                    }).OrderBy(p => p.Product).ToList();
                    dt.Columns.Add("Product Name",typeof(string));
                    dt.Columns.Add("Quantity Added", typeof(int));
                    dt.Columns.Add("Quantity Removed", typeof(int));
                    dt.Columns.Add("Total Remaining", typeof(int));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.QuantityAdded,
                            sale.QuantityRemoved, sale.CurrentStock);
                    }
                }
                var resultform = new ResultView(dt);
                resultform.ShowDialog();
            }
            else
            {
                MessageBox.Show(@"Select Any Dimensions");
            }
        }
    }
}
