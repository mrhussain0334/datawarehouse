﻿using System;

namespace OLAP.View
{
    public class SaleView
    {
        public int Sort { get; set; }
        public DateTime SortDateTime { get; set; }

        public string Name { get; set; }
        public string DateTime { get; set; }
        public float TotalDiscount { get; set; }
        public float TotalSale { get; set; }
        public float TotalQuantity { get; set; }
        public string Product { get; set; }
    }
}
