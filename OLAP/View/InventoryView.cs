﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLAP.View
{
    public class InventoryView
    {
        public string Product { get; set; }
        public string DateTime { get; set; }
        public int Sort { get; set; }
        public DateTime SortDateTime { get; set; }
        public int QuantityAdded { get; set; }
        public int QuantityRemoved { get; set; }
        public int CurrentStock { get; set; }
    }
}
