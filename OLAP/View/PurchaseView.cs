﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLAP.View
{
    public class PurchaseView
    {
        public string SupplierName { get; set; }
        public string Product { get; set; }
        public string DateTime { get; set; }
        public int Sort { get; set; }
        public DateTime SortDateTime { get; set; }
        public float TotalDiscount { get; set; }
        public float TotalCost { get; set; }
        public float TotalQuantity { get; set; }
    }
}
