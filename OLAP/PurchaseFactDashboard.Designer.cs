﻿namespace OLAP
{
    partial class PurchaseFactDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbDateSearch = new System.Windows.Forms.RadioButton();
            this.rbDateAll = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbProductSearch = new System.Windows.Forms.RadioButton();
            this.rbProductAll = new System.Windows.Forms.RadioButton();
            this.cbDate = new System.Windows.Forms.CheckBox();
            this.cbProduct = new System.Windows.Forms.CheckBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cbxDateWise = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductSearch = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSupplierSearch = new System.Windows.Forms.RadioButton();
            this.rbSupplierAll = new System.Windows.Forms.RadioButton();
            this.cbSupplier = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSupplierSearch = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbDateSearch);
            this.groupBox3.Controls.Add(this.rbDateAll);
            this.groupBox3.Location = new System.Drawing.Point(216, 181);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(124, 68);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Date";
            // 
            // rbDateSearch
            // 
            this.rbDateSearch.AutoSize = true;
            this.rbDateSearch.Location = new System.Drawing.Point(6, 20);
            this.rbDateSearch.Name = "rbDateSearch";
            this.rbDateSearch.Size = new System.Drawing.Size(121, 17);
            this.rbDateSearch.TabIndex = 14;
            this.rbDateSearch.TabStop = true;
            this.rbDateSearch.Text = "Include Date Range";
            this.rbDateSearch.UseVisualStyleBackColor = true;
            // 
            // rbDateAll
            // 
            this.rbDateAll.AutoSize = true;
            this.rbDateAll.Location = new System.Drawing.Point(6, 40);
            this.rbDateAll.Name = "rbDateAll";
            this.rbDateAll.Size = new System.Drawing.Size(100, 17);
            this.rbDateAll.TabIndex = 15;
            this.rbDateAll.TabStop = true;
            this.rbDateAll.Text = "Include All Date";
            this.rbDateAll.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbProductSearch);
            this.groupBox2.Controls.Add(this.rbProductAll);
            this.groupBox2.Location = new System.Drawing.Point(311, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(140, 71);
            this.groupBox2.TabIndex = 53;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Products";
            // 
            // rbProductSearch
            // 
            this.rbProductSearch.AutoSize = true;
            this.rbProductSearch.Location = new System.Drawing.Point(7, 19);
            this.rbProductSearch.Name = "rbProductSearch";
            this.rbProductSearch.Size = new System.Drawing.Size(137, 17);
            this.rbProductSearch.TabIndex = 10;
            this.rbProductSearch.TabStop = true;
            this.rbProductSearch.Text = "Include Product Search";
            this.rbProductSearch.UseVisualStyleBackColor = true;
            // 
            // rbProductAll
            // 
            this.rbProductAll.AutoSize = true;
            this.rbProductAll.Location = new System.Drawing.Point(7, 42);
            this.rbProductAll.Name = "rbProductAll";
            this.rbProductAll.Size = new System.Drawing.Size(114, 17);
            this.rbProductAll.TabIndex = 11;
            this.rbProductAll.TabStop = true;
            this.rbProductAll.Text = "Include All Product";
            this.rbProductAll.UseVisualStyleBackColor = true;
            // 
            // cbDate
            // 
            this.cbDate.AutoSize = true;
            this.cbDate.Location = new System.Drawing.Point(355, 232);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(104, 17);
            this.cbDate.TabIndex = 52;
            this.cbDate.Text = "Date  Dimension";
            this.cbDate.UseVisualStyleBackColor = true;
            this.cbDate.CheckedChanged += new System.EventHandler(this.cbDate_CheckedChanged);
            // 
            // cbProduct
            // 
            this.cbProduct.AutoSize = true;
            this.cbProduct.Location = new System.Drawing.Point(457, 29);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(118, 17);
            this.cbProduct.TabIndex = 51;
            this.cbProduct.Text = "Product  Dimension";
            this.cbProduct.UseVisualStyleBackColor = true;
            this.cbProduct.CheckedChanged += new System.EventHandler(this.cbProduct_CheckedChanged);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(11, 301);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(124, 23);
            this.btnProcess.TabIndex = 50;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cbxDateWise
            // 
            this.cbxDateWise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDateWise.FormattingEnabled = true;
            this.cbxDateWise.Items.AddRange(new object[] {
            "Select Date Wise",
            "Year Wise",
            "Month Wise",
            "Week Wise",
            "Day Wise"});
            this.cbxDateWise.Location = new System.Drawing.Point(355, 199);
            this.cbxDateWise.Name = "cbxDateWise";
            this.cbxDateWise.Size = new System.Drawing.Size(206, 21);
            this.cbxDateWise.TabIndex = 49;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 48;
            this.label5.Text = "To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 47;
            this.label4.Text = "From";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(10, 247);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 20);
            this.dtpTo.TabIndex = 46;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(10, 199);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpFrom.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 168);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "Date Range";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Product Search";
            // 
            // txtProductSearch
            // 
            this.txtProductSearch.Location = new System.Drawing.Point(12, 29);
            this.txtProductSearch.Name = "txtProductSearch";
            this.txtProductSearch.Size = new System.Drawing.Size(293, 20);
            this.txtProductSearch.TabIndex = 42;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbSupplierSearch);
            this.groupBox1.Controls.Add(this.rbSupplierAll);
            this.groupBox1.Location = new System.Drawing.Point(315, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(140, 71);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Products";
            // 
            // rbSupplierSearch
            // 
            this.rbSupplierSearch.AutoSize = true;
            this.rbSupplierSearch.Location = new System.Drawing.Point(7, 19);
            this.rbSupplierSearch.Name = "rbSupplierSearch";
            this.rbSupplierSearch.Size = new System.Drawing.Size(138, 17);
            this.rbSupplierSearch.TabIndex = 10;
            this.rbSupplierSearch.TabStop = true;
            this.rbSupplierSearch.Text = "Include Supplier Search";
            this.rbSupplierSearch.UseVisualStyleBackColor = true;
            // 
            // rbSupplierAll
            // 
            this.rbSupplierAll.AutoSize = true;
            this.rbSupplierAll.Location = new System.Drawing.Point(7, 42);
            this.rbSupplierAll.Name = "rbSupplierAll";
            this.rbSupplierAll.Size = new System.Drawing.Size(115, 17);
            this.rbSupplierAll.TabIndex = 11;
            this.rbSupplierAll.TabStop = true;
            this.rbSupplierAll.Text = "Include All Supplier";
            this.rbSupplierAll.UseVisualStyleBackColor = true;
            // 
            // cbSupplier
            // 
            this.cbSupplier.AutoSize = true;
            this.cbSupplier.Location = new System.Drawing.Point(461, 106);
            this.cbSupplier.Name = "cbSupplier";
            this.cbSupplier.Size = new System.Drawing.Size(119, 17);
            this.cbSupplier.TabIndex = 57;
            this.cbSupplier.Text = "Supplier  Dimension";
            this.cbSupplier.UseVisualStyleBackColor = true;
            this.cbSupplier.CheckedChanged += new System.EventHandler(this.cbSupplier_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 56;
            this.label1.Text = "Supplier Search";
            // 
            // txtSupplierSearch
            // 
            this.txtSupplierSearch.Location = new System.Drawing.Point(16, 106);
            this.txtSupplierSearch.Name = "txtSupplierSearch";
            this.txtSupplierSearch.Size = new System.Drawing.Size(293, 20);
            this.txtSupplierSearch.TabIndex = 55;
            // 
            // PurchaseFactDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 339);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbSupplier);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSupplierSearch);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.cbProduct);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.cbxDateWise);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProductSearch);
            this.Name = "PurchaseFactDashboard";
            this.Text = "PurchaseFactDashboard";
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbDateSearch;
        private System.Windows.Forms.RadioButton rbDateAll;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbProductSearch;
        private System.Windows.Forms.RadioButton rbProductAll;
        private System.Windows.Forms.CheckBox cbDate;
        private System.Windows.Forms.CheckBox cbProduct;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ComboBox cbxDateWise;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSupplierSearch;
        private System.Windows.Forms.RadioButton rbSupplierAll;
        private System.Windows.Forms.CheckBox cbSupplier;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSupplierSearch;
    }
}