﻿namespace OLAP
{
    partial class InventoryFactDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbDateSearch = new System.Windows.Forms.RadioButton();
            this.rbDateAll = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbProductSearch = new System.Windows.Forms.RadioButton();
            this.rbProductAll = new System.Windows.Forms.RadioButton();
            this.cbDate = new System.Windows.Forms.CheckBox();
            this.cbProduct = new System.Windows.Forms.CheckBox();
            this.btnProcess = new System.Windows.Forms.Button();
            this.cbxDateWise = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtProductSearch = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbDateSearch);
            this.groupBox3.Controls.Add(this.rbDateAll);
            this.groupBox3.Location = new System.Drawing.Point(216, 90);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(124, 68);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Date";
            // 
            // rbDateSearch
            // 
            this.rbDateSearch.AutoSize = true;
            this.rbDateSearch.Location = new System.Drawing.Point(6, 20);
            this.rbDateSearch.Name = "rbDateSearch";
            this.rbDateSearch.Size = new System.Drawing.Size(121, 17);
            this.rbDateSearch.TabIndex = 14;
            this.rbDateSearch.TabStop = true;
            this.rbDateSearch.Text = "Include Date Range";
            this.rbDateSearch.UseVisualStyleBackColor = true;
            // 
            // rbDateAll
            // 
            this.rbDateAll.AutoSize = true;
            this.rbDateAll.Location = new System.Drawing.Point(6, 40);
            this.rbDateAll.Name = "rbDateAll";
            this.rbDateAll.Size = new System.Drawing.Size(100, 17);
            this.rbDateAll.TabIndex = 15;
            this.rbDateAll.TabStop = true;
            this.rbDateAll.Text = "Include All Date";
            this.rbDateAll.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbProductSearch);
            this.groupBox2.Controls.Add(this.rbProductAll);
            this.groupBox2.Location = new System.Drawing.Point(311, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(140, 71);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Products";
            // 
            // rbProductSearch
            // 
            this.rbProductSearch.AutoSize = true;
            this.rbProductSearch.Location = new System.Drawing.Point(7, 19);
            this.rbProductSearch.Name = "rbProductSearch";
            this.rbProductSearch.Size = new System.Drawing.Size(137, 17);
            this.rbProductSearch.TabIndex = 10;
            this.rbProductSearch.TabStop = true;
            this.rbProductSearch.Text = "Include Product Search";
            this.rbProductSearch.UseVisualStyleBackColor = true;
            // 
            // rbProductAll
            // 
            this.rbProductAll.AutoSize = true;
            this.rbProductAll.Location = new System.Drawing.Point(7, 42);
            this.rbProductAll.Name = "rbProductAll";
            this.rbProductAll.Size = new System.Drawing.Size(114, 17);
            this.rbProductAll.TabIndex = 11;
            this.rbProductAll.TabStop = true;
            this.rbProductAll.Text = "Include All Product";
            this.rbProductAll.UseVisualStyleBackColor = true;
            // 
            // cbDate
            // 
            this.cbDate.AutoSize = true;
            this.cbDate.Location = new System.Drawing.Point(355, 141);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(104, 17);
            this.cbDate.TabIndex = 39;
            this.cbDate.Text = "Date  Dimension";
            this.cbDate.UseVisualStyleBackColor = true;
            this.cbDate.CheckedChanged += new System.EventHandler(this.cbDate_CheckedChanged);
            // 
            // cbProduct
            // 
            this.cbProduct.AutoSize = true;
            this.cbProduct.Location = new System.Drawing.Point(457, 29);
            this.cbProduct.Name = "cbProduct";
            this.cbProduct.Size = new System.Drawing.Size(118, 17);
            this.cbProduct.TabIndex = 38;
            this.cbProduct.Text = "Product  Dimension";
            this.cbProduct.UseVisualStyleBackColor = true;
            this.cbProduct.CheckedChanged += new System.EventHandler(this.cbProduct_CheckedChanged);
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(11, 210);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(124, 23);
            this.btnProcess.TabIndex = 37;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cbxDateWise
            // 
            this.cbxDateWise.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDateWise.FormattingEnabled = true;
            this.cbxDateWise.Items.AddRange(new object[] {
            "Select Date Wise",
            "Year Wise",
            "Month Wise",
            "Week Wise",
            "Day Wise"});
            this.cbxDateWise.Location = new System.Drawing.Point(355, 108);
            this.cbxDateWise.Name = "cbxDateWise";
            this.cbxDateWise.Size = new System.Drawing.Size(206, 21);
            this.cbxDateWise.TabIndex = 36;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 35;
            this.label5.Text = "To";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "From";
            // 
            // dtpTo
            // 
            this.dtpTo.Location = new System.Drawing.Point(10, 156);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(200, 20);
            this.dtpTo.TabIndex = 33;
            // 
            // dtpFrom
            // 
            this.dtpFrom.Location = new System.Drawing.Point(10, 108);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(200, 20);
            this.dtpFrom.TabIndex = 32;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 31;
            this.label3.Text = "Date Range";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Product Search";
            // 
            // txtProductSearch
            // 
            this.txtProductSearch.Location = new System.Drawing.Point(12, 29);
            this.txtProductSearch.Name = "txtProductSearch";
            this.txtProductSearch.Size = new System.Drawing.Size(293, 20);
            this.txtProductSearch.TabIndex = 29;
            // 
            // InventoryFactDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(591, 328);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.cbProduct);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.cbxDateWise);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dtpTo);
            this.Controls.Add(this.dtpFrom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProductSearch);
            this.Name = "InventoryFactDashboard";
            this.Text = "InventoryFactDashboard";
            this.Load += new System.EventHandler(this.InventoryFactDashboard_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbDateSearch;
        private System.Windows.Forms.RadioButton rbDateAll;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rbProductSearch;
        private System.Windows.Forms.RadioButton rbProductAll;
        private System.Windows.Forms.CheckBox cbDate;
        private System.Windows.Forms.CheckBox cbProduct;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.ComboBox cbxDateWise;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtProductSearch;
    }
}