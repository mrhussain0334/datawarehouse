﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DataContext;
using OLAP.View;

namespace OLAP
{
    public partial class SalesFactDashbaord : Form
    {
        public SalesFactDashbaord()
        {
            InitializeComponent();
            Initialize();
        }


        private void Initialize()
        {
            cbUser.Checked = false;
            cbProduct.Checked = false;
            rbUserAll.Checked = true;
            rbProductAll.Checked = true;
            rbDateAll.Checked = true;
            cbxDateWise.SelectedIndex = 0;
            txtUserSearch.Enabled = rbUserAll.Enabled = rbUserSearch.Enabled = false;
            txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
        }
        private void cbUser_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbUser.Checked)
            {
                txtUserSearch.Enabled = rbUserAll.Enabled = rbUserSearch.Enabled = false;
            }
            else
            {
                txtUserSearch.Enabled = rbUserAll.Enabled = rbUserSearch.Enabled = true;
            }
        }

        private void cbProduct_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbProduct.Checked)
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = false;
            }
            else
            {
                txtProductSearch.Enabled = rbProductAll.Enabled = rbProductSearch.Enabled = true;
            }
        }

        private void cbDate_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbDate.Checked)
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = false;
            }
            else
            {
                dtpFrom.Enabled = dtpTo.Enabled = rbDateAll.Enabled = rbDateSearch.Enabled = cbxDateWise.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var result = new List<SaleView>();
            var dt = new DataTable("Result");
            if (cbUser.Checked || cbDate.Checked || cbProduct.Checked)
            {
                if (cbUser.Checked && cbDate.Checked && cbProduct.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var productFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbUserAll.Checked)
                    {
                        userFilter = txtUserSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    if (!rbProductAll.Checked)
                    {
                        productFilter = txtProductSearch.Text;
                    }

                    var list =
                        datawarehouse.UserProductDateSaleFactCubes.Where(
                            p =>
                                p.UserAccount.FullName.Contains(userFilter) &&
                                p.Product.Name.Contains(productFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    dt.Columns.Add("Full Name", typeof(string));
                    dt.Columns.Add("Product Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new { p.UserId,p.ProductId, p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Date.Year + "",
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.UserId,p.ProductId, p.DateTable.Month,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.UserId,p.Product.Id, p.DateTable.Week,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new SaleView()
                        {
                            DateTime = p.DateTable.Date.ToShortDateString(),
                            Name = p.UserAccount.FullName,
                            Product = p.Product.Name,
                            TotalDiscount = p.TotalDiscount,
                            TotalQuantity = p.TotalQuantity,
                            TotalSale = p.TotalSale,
                            SortDateTime = p.DateTable.Date,
                        }).OrderBy(p => p.SortDateTime).ToList();

                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Name,sale.Product, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbUser.Checked && cbDate.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbUserAll.Checked)
                    {
                        userFilter = txtUserSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.UserDateSaleFactCubes.Where(
                            p =>
                                p.UserAccount.FullName.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    dt.Columns.Add("Full Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new {p.UserId, p.DateTable.Year}).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Date.Year + "",
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Year,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new {p.UserId, p.DateTable.Month,p.DateTable.Year}).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Month,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new {p.UserId, p.DateTable.Week, p.DateTable.Year}).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Name = p.FirstOrDefault().UserAccount.FullName,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Week,
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new SaleView()
                        {
                            DateTime = p.DateTable.Date.ToShortDateString(),
                            Name = p.UserAccount.FullName,
                            TotalDiscount = p.TotalDiscount,
                            TotalQuantity = p.TotalQuantity,
                            TotalSale = p.TotalSale,
                            SortDateTime = p.DateTable.Date,
                        }).OrderBy(p => p.SortDateTime).ToList();
                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Name, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbUser.Checked && cbProduct.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var productFilter = "";
                    if (!rbUserAll.Checked)
                    {
                        userFilter = txtUserSearch.Text.Trim();
                    }
                    if (!rbProductAll.Checked)
                    {
                        productFilter = txtProductSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.UserProductSaleFacts.Where(
                            p =>
                                p.UserAccount.FullName.Contains(userFilter) &&
                                p.Product.Name.Contains(productFilter)).ToList();
                    result = list.Select(p => new SaleView()
                    {
                        Name = p.UserAccount.FullName,
                        Product = p.Product.Name,
                        TotalDiscount = p.TotalDiscount,
                        TotalQuantity = p.TotalQuantity,
                        TotalSale = p.TotalSale
                    }).OrderBy(p => p.Name).ToList();
                    dt.Columns.Add("Full Name", typeof(string));
                    dt.Columns.Add("Product", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Name, sale.Product, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbDate.Checked && cbProduct.Checked)
                {
                    if (cbxDateWise.SelectedIndex <= 0)
                    {
                        MessageBox.Show(@"Select Date Wise");
                        return;
                    }
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.ProductDateSaleFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter) &&
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    dt.Columns.Add("Product Name", typeof(string));
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Year + "",
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Year
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Month,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Month
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.ProductId, p.DateTable.Week,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            Product = p.FirstOrDefault().Product.Name,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Week
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new SaleView()
                        {
                            DateTime = p.DateTable.Date.ToShortDateString(),
                            Product = p.Product.Name,
                            TotalDiscount = p.TotalDiscount,
                            TotalQuantity = p.TotalQuantity,
                            TotalSale = p.TotalSale,
                            SortDateTime = p.DateTable.Date
                        }).OrderBy(p => p.SortDateTime).ToList();

                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbDate.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var startDate = DateTime.MinValue;
                    var endDate = DateTime.MaxValue;
                    if (!rbDateAll.Checked)
                    {
                        startDate = dtpFrom.Value;
                        endDate = dtpTo.Value;
                    }
                    var list =
                        datawarehouse.DateSaleFactsCubes.Where(
                            p =>
                                (p.DateTable.Date >= startDate && p.DateTable.Date <= endDate)).ToList();
                    if (cbxDateWise.SelectedIndex == 1)
                    {
                        dt.Columns.Add("Year", typeof(int));

                        result = list.GroupBy(p => new {p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Date.Year + "",
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Year
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 2)
                    {
                        dt.Columns.Add("Month", typeof(string));
                        result = list.GroupBy(p => new { p.DateTable.Month,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Month + "/" + p.FirstOrDefault().DateTable.Year,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Month
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 3)
                    {
                        dt.Columns.Add("Week", typeof(string));
                        result = list.GroupBy(p => new { p.DateTable.Week,p.DateTable.Year }).Select(p => new SaleView()
                        {
                            DateTime = p.FirstOrDefault().DateTable.Week + "/" + p.FirstOrDefault().DateTable.Year,
                            TotalDiscount = p.Sum(asc => asc.TotalDiscount),
                            TotalQuantity = p.Sum(asc => asc.TotalQuantity),
                            TotalSale = p.Sum(asc => asc.TotalSale),
                            Sort = p.FirstOrDefault().DateTable.Week
                        }).OrderBy(p => p.Sort).ToList();

                    }
                    else if (cbxDateWise.SelectedIndex == 4)
                    {
                        dt.Columns.Add("Date", typeof(string));
                        result = list.Select(p => new SaleView()
                        {
                            DateTime = p.DateTable.Date.ToShortDateString(),
                            TotalDiscount = p.TotalDiscount,
                            TotalQuantity = p.TotalQuantity,
                            TotalSale = p.TotalSale,
                            SortDateTime = p.DateTable.Date
                        }).OrderBy(p => p.SortDateTime).ToList();

                    }
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.DateTime, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbProduct.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    if (!rbProductAll.Checked)
                    {
                        userFilter = txtProductSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.ProductSaleFactCubes.Where(
                            p =>
                                p.Product.Name.Contains(userFilter));
                    result = list.Select(p => new SaleView()
                    {
                        Product = p.Product.Name,
                        TotalDiscount = p.TotalDiscount,
                        TotalQuantity = p.TotalQuantity,
                        TotalSale = p.TotalSale,
                    }).OrderBy(p => p.Product).ToList();
                    dt.Columns.Add("Product Name", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Product, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                else if (cbUser.Checked)
                {
                    var datawarehouse = new Datawarehouse();
                    var userFilter = "";
                    if (!rbUserAll.Checked)
                    {
                        userFilter = txtUserSearch.Text.Trim();
                    }
                    var list =
                        datawarehouse.UserSaleFactCubes.Where(
                            p =>
                                p.UserAccount.FullName.Contains(userFilter));
                    result = list.Select(p => new SaleView()
                    {
                        Name = p.UserAccount.FullName,
                        TotalDiscount = p.TotalDiscount,
                        TotalQuantity = p.TotalQuantity,
                        TotalSale = p.TotalSale,
                    }).OrderBy(p => p.Name).ToList();
                    dt.Columns.Add("User Name", typeof(string));
                    dt.Columns.Add("Total Discount", typeof(float));
                    dt.Columns.Add("Total Quantity", typeof(int));
                    dt.Columns.Add("Total Sale", typeof(float));
                    foreach (var sale in result)
                    {
                        dt.Rows.Add(sale.Name, sale.TotalDiscount,
                            sale.TotalQuantity, sale.TotalSale);
                    }
                }
                var resultform = new ResultView(dt);
                resultform.ShowDialog();
            }
            else
            {
                MessageBox.Show(@"Select Max 2 and Min 1 Dimesnion");
            }
        }

        private void SalesFactDashbaord_Load(object sender, EventArgs e)
        {

        }
    }
}
